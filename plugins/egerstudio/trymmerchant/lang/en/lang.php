<?php

return [
    'plugin'                          => [
      'name'                          =>          'Nettordre',
      'description'                   =>          'Hyndla fra Eger Studio'
    ],

    'component'                       => [
      'name'                          =>          '',
      'description'                   =>          ''
    ],

    'permissions'                     => [
      'manage_customers'              =>          'Administrere kunder',
      'manage_menus'                  =>          'Administrere menyer',
      'manage_orders'                 =>          'Administrere ordre',
      'manage_products'               =>          'Administrere produkter',
      'manage_schedules'              =>          'Administrere timeplaner',
      'manage_specialdays'            =>          'Administrere datoer',
      'manage_companies'              =>          'Administrere firmaer',
    ],
    'title'                           => [
      'list'                          => [
        'customers'                   =>          'Kunder',
        'days'                        =>          'Dager',
        'menus'                       =>          'Menyer',
        'orderlines'                  =>          'Varelinjer',
        'orders'                      =>          'Ordre',
        'productcategories'           =>          'Varegrupper',
        'products'                    =>          'Varer',
        'schedules'                   =>          'Timeplaner'
      ],
      'form'                          => [
        'create'                      => [
          'customers'                 =>          'Opprett kunde',
          'days'                      =>          'Opprett dag',
          'menus'                     =>          'Opprett meny',
          'orderlines'                =>          'Opprett varelinje',
          'orders'                    =>          'Opprett ordre',
          'productcategories'         =>          'Opprett varegruppe',
          'products'                  =>          'Opprett vare',
          'schedules'                 =>          'Opprett timeplan',
          'specialdays'               =>          'Opprett spesialdag'
        ],
        'update'                      => [
          'customers'                 =>          'Rediger kunde',
          'days'                      =>          'Rediger dag',
          'menus'                     =>          'Rediger meny',
          'orderlines'                =>          'Rediger varelinje',
          'orders'                    =>          'Rediger ordre',
          'productcategories'         =>          'Rediger varegruppe',
          'products'                  =>          'Rediger vare',
          'schedules'                 =>          'Rediger timeplan',
          'specialdays'               =>          'Rediger spesialdag'
        ],
        'preview'                     => [
          'customers'                 =>          'Forhåndsvis kunde',
          'days'                      =>          'Forhåndsvis dag',
          'menus'                     =>          'Forhåndsvis meny',
          'orderlines'                =>          'Forhåndsvis varelinje',
          'orders'                    =>          'Forhåndsvis ordre',
          'productcategories'         =>          'Forhåndsvis varegruppe',
          'products'                  =>          'Forhåndsvis vare',
          'schedules'                 =>          'Forhåndsvis timeplan',
          'specialdays'               =>          'Forhåndsvis spesialdag'
        ],
        'manage'                      =>  [
          'specialdays'               =>          'Administrere spesialdatoer',
        ],
      ],
    ],
    'toolbar'                         => [
      'customer'                      => [
        'new'                         =>          'Ny kunde'
      ],
      'company'                      => [
        'new'                         =>          'Nytt firma'
      ],
      'menu'                          => [
        'new'                         =>          'Ny meny',
      ],
      'order'                         => [
        'new'                         =>          'Opprett ordre manuelt',
      ],
      'orderline'                     => [
        'new'                         =>          'Ny varelinje',
        'create'                      =>          'Opprett varelinje',
      ],
      'product'                       => [
        'new'                         =>          'Nytt produkt',
      ],
      'productcategory'               => [
        'new'                         =>          'Ny varegruppe',
      ],
      'schedule'                      => [
        'new'                         =>          'Ny timeplan',
      ],
      'specialday'                      => [
        'new'                         =>          'Ny spesialdato',
      ],
    ],
    'buttons'                         => [
      'save'                          =>          'Lagre',
      'saveAndClose'                  =>          'Lagre og lukk',
      'or'                            =>          'eller',
      'cancel'                        =>          'avbryt',
      'return'                        =>          'Tilbake'
    ],
    'breadcrumbs'                     => [
      'customer'                      => [
        'breadcrumb'                  =>          'Kunde',
        'new'                         =>          'Ny kunde',
        'edit'                        =>          'Rediger opplysninger',
      ],
      'company'                      => [
        'breadcrumb'                  =>          'Firma',
        'new'                         =>          'Nytt firma',
        'edit'                        =>          'Rediger opplysninger',
      ],
      'menu'                          => [
        'breadcrumb'                  =>          'Meny',
        'new'                         =>          'Ny meny',
        'edit'                        =>          'Rediger meny',
      ],
      'order'                         => [
        'breadcrumb'                  =>          'Ordre',
        'new'                         =>          'Ny ordre',
        'edit'                        =>          'Rediger ordre',
      ],
      'productcategory'               => [
        'breadcrumb'                  =>          'Varegruppe',
        'new'                         =>          'Ny varegruppe',
        'edit'                        =>          'Rediger varegruppe',
      ],
      'product'                       => [
        'breadcrumb'                  =>          'Vare',
        'new'                         =>          'Ny vare',
        'edit'                        =>          'Rediger vare',
      ],
      'schedule'                      => [
        'breadcrumb'                  =>          'Timeplan',
        'new'                         =>          'Ny timeplan',
        'edit'                        =>          'Rediger timeplan',
      ],
      'specialday'                      => [
        'breadcrumb'                  =>          'Dato',
        'new'                         =>          'Ny spesialdato',
        'edit'                        =>          'Rediger dato',
      ],
    ],
    'confirm'                         => [
      'delete'                        => [
        'customer'                    =>          'Er du sikker på at du vil slette kunden?',
        'menu'                        =>          'Er du sikker på at du vil slette menyen?',
        'order'                       =>          'Er du sikker på at du vil slette ordren?',
        'productcategory'             =>          'Er du sikker på at du vil slette varegruppen?',
        'product'                     =>          'Er du sikker på at du vil slette varen?',
        'schedule'                    =>          'Er du sikker på at du vil slette timeplanen?',
        'specialday'                  =>          'Er du sikker på at du vil slette denne datoen?'
      ],
    ],
    'status'                          => [
      'saving'                        =>          'Lagrer...',
      'deleting'                      =>          'Sletter...',
    ],
    'flashMessages'                   => [

    ],
    'navigation'                      => [
        'customers'                   =>          'Kunder',
        'menus'                       =>          'Menyer',
        'orders'                      =>          'Ordre',
        'products'                    =>          'Varer',
        'productcategories'           =>          'Varegrupper',
        'schedules'                   =>          'Timeplaner',
        'specialdays'                 =>          'Datoer',
        'companies'                   =>          'Firmaer',
    ],

    'tabs'                            => [
        'customer'                    => [

        ],
        'company'                     => [
          'address'                   =>          'Kontaktinformasjon',
          'inv_address'               =>          'Fakturadetaljer',
          'contact'                   =>          'Kontaktperson',
          'customers'                 =>          'Kunder som kan bestille',
          'schedules'                 =>          'Aktive menyer'
        ],
        'menu'                        => [
          'details'                   =>          'Detaljer',
          'products'                  =>          'Varer',
          'text'                      =>          'Tekst',
        ],
        'order'                       => [
          'details'                   =>          'Detaljer',
          'orderlines'                =>          'Varelinjer',
          'logs'                      =>          'Ordrelogg',
          'misc'                      =>          'Diverse',
          'company'                   =>          'Firma'
        ],
        'product'                     => [

        ],
        'productcategory'             => [

        ],
        'schedule'                    => [
          'details'                   =>           'Detaljer',
          'days'                      =>           'Dager',
          'menus'                     =>           'Menyer',
          'trailing'                  =>           'Tekster',
        ],
        'settings'                    =>  [
          'basic'                     =>           'Innstillinger',
          'messaging'                 =>           'Meldingsoppsett',
          'printservices'             =>           'Skrivertjenester',
          'openinghours'              =>           'Åpningstider',
          'conditions'                =>           'Brukerbetingelser',
          'textblocks'                =>           'Tekster'
        ],
      ],

    'customer'                        => [
      'firstname'                     =>           'Fornavn',
      'lastname'                      =>           'Etternavn',
      'phone'                         =>           'Mobil',
      'email'                         =>           'E-post',
      'active'                        =>           'Aktiv',
      'returnToList'                  =>           'Tilbake til kundeoversikten',
      'companies'                     =>           'Firma'
    ],
    'company'                         => [
      'title'                         =>           'Firmanavn',
      'address'                       =>           'Adresse',
      'zip'                           =>           'Postnummer',
      'city'                          =>           'Sted',
      'inv_address'                   =>           'Fakturaadresse',
      'inv_zip'                       =>           'Faktura postnummer',
      'inv_city'                      =>           'Faktura sted',
      'phone'                         =>           'Telefon',
      'email'                         =>           'E-post',
      'website'                       =>           'Nettsted',
      'company_id'                    =>           'Organisasjonsnummer',
      'primary_contact'               =>           'Kontaktperson',
      'primary_conatct_phone'         =>           'Kontaktperson telefon',
      'primary_conatct_email'         =>           'Kontaktperson e-post',
      'inv_email'                     =>           'Faktura e-post',
      'alias'                         =>           'Navn på bongskriver',
      'is_active'                     =>           'Aktiv',
    ],
    'days'                            => [
      'day'                           =>           'Dag',
      'dayno'                         =>           'Rekkefølge (0-6)'
    ],
    'specialdays'                     => [
      'date'                          =>           'Dato',
      'title'                         =>           'Tittel',
      'comment'                       =>           'Kommentar',
      'open'                          =>           'Åpne',
      'close'                         =>           'Stenge',
      'closed'                        =>           'Helt stengt',
      'active'                        =>           'Aktivert',
      'publictext'                    =>           'Tekst til nettsiden'
    ],
    'log'                             => [
      'log_text'                      =>           'Logg info',
      'created_at'                    =>           'Logg tid'
    ],
    'menu'                            => [
      'title'                         =>           'Menynavn',
      'description'                   =>           'Beskrivelse',
      'slug'                          =>           'Slug',
      'isActive'                      =>           'Aktiv',
      'sortOrder'                     =>           'Sortering',
      'note'                          =>           'Notat',
      'products'                      =>           'Vare',
      'details'                       =>           'Tekst under meny',
      'details_top'                   =>           'Tekst over meny',
    ],
    'order'                           => [
      'id'                            =>           'Hentenummer',
      'comment'                       =>           'Kommentar',
      'price'                         =>           'Pris',
      'verification_code'             =>           'Bekreftelseskode',
      'customer_code'                 =>           'Kode fra kunde',
      'customer_code_comment'         =>           'Viser koden kunde sendte inn som bekreftelse',
      'verified'                      =>           'Verifisert',
      'verified_comment'              =>           'Viser om ordren er verifisert med kode fra kunde',
      'confirmed'                     =>           'Bekreftet',
      'confirmed_comment'             =>           'Viser om ordren er bekreftet fra Dashboardet',
      'customer'                      =>           'Kunde',
      'pick_up_time'                  =>           'Hentetidspunkt',
      'order_id'                      =>           'Ordre',
      'customer'                      =>           'Kunde',
      'picked_up_ind'                 =>           'Varene er hentet',
      'picked_up_ind_comment'         =>           'Sett til AV for å sende ordren tilbake til Dashboardet',
      'active'                        =>           'Aktivér ordre',
      'active_comment'                =>           'Tvinger aktivering av en ordre i systemet. Brukes ved nye og deaktiverte ordre.',
      'orderlines'                    =>           'Varelinjer',
      'logs'                          =>           'Ordrelogg',
      'filter_verified'               =>           'Vis kun bekreftede ordre',
      'company'                       =>           'Firma',
    ],
    'orderline'                       => [
      'product'                       =>           'Vare',
      'quantity'                      =>           'Antall',
    ],
    'product'                         => [
      'name'                          =>           'Navn',
      'description'                   =>           'Beskrivelse',
      'allergens'                     =>           'Allergener',
      'allergens_hide'                =>           'Skjul allergener',
      'price'                         =>           'Pris',
      'active'                        =>           'Aktiv',
      'category'                      =>           'Kategori',
      'sort_order'                    =>           'Sortering',
      'weborder'                      =>           'Online',
      'time_to_prepare'               =>           'Tid å lage'
    ],

    'productcategory'                 => [
      'name'                          =>           'Navn',
      'description'                   =>           'Beskrivelse',
      'active'                        =>           'Aktiv',
      'sort_order'                    =>           'Sortering'
    ],
    'promo'                           => [
      'name'                          =>           'Navn',
      'description'                   =>           'Beskrivelse',
      'active'                        =>           'Aktiv'
    ],
    'schedule'                        =>  [
      'title'                         =>           'Navn på timeplan',
      'description'                   =>           'Beskrivelse',
      'isActive'                      =>           'Aktiv',
      'start_time'                    =>           'Starttid',
      'start_time_description'        =>           'Skriv inn som 10:00',
      'end_time'                      =>           'Utløpstid',
      'end_time_description'          =>           'Skriv inn som 10:00',
      'disablePriceDisplay'           =>           'Ikke vis priser i denne listen',
      'excemptFromRegular'            =>           'Ikke vis denne planen i vanlig utlisting',
      'weekdays'                      =>           'Ukedager',
      'menus'                         =>           'Menyer',
      'sort_order'                    =>           'Sortering',
      'trailing_title'                =>           'Overskrift bong',
      'trailing_text'                 =>           'Tekst til bong'
    ],
    //front

    'merchantbasket'                  => [
      'title'                         =>           'Tittel',

    ],


    // settings
    'settings'                        => [
      'name'                          =>           'Hyndla nettordre',
      'client_id'                     =>           'Din AppID',
      'client_id_comment'             =>           'Du trenger denne for å identifisere deg med applikasjonsserveren',
      'secret_key'                    =>           'Hemmelig nøkkel',
      'secret_key_comment'            =>           'Passord brukt for tilgang til applikasjonsserver. IKKE DEL MED NOEN',
      'vanity_name_short'             =>           'Kort navn på virksomheten',
      'vanity_name_short_comment'     =>           'Brukes som avsender på SMS',
      'vanity_name_long'              =>           'Avsender e-post',
      'vanity_name_receipt'           =>           'Navn på bonger',
      'vanity_name_receipt_comment'   =>           'Øverste linje på bongene fra bestillingssystemet',
      'vanity_name_web'               =>           'Bonglinje info',
      'vanity_name_web_comment'       =>           'Nettadresse eller fullt navn på bonger',
      'cast_to_phone'                 =>           'Send ordre til mobiltelefon',
      'cast_to_phone_comment'         =>           'Sett til PÅ hvis du vil at systemet sender ordre direkte til angitt mobil',
      'cast_to_phone_number'          =>           'Mobilnummer for ordremottak',
      'cast_to_phone_number_comment'  =>           'OBS, ingen landskode, kun norsk mobilnummer',
      'cast_to_email'                 =>           'Send ordre til e-post',
      'cast_to_email_comment'         =>           'Sett til PÅ hvis du vil at systemet sender ordre til angitt e-post adresse',
      'cast_to_email_email'           =>           'Skriv inn e-post adressen du vil motta ordre på',
      'cast_to_email_email_comment'   =>           'Fullstendig e-post adresse for ordre',
      'cast_to_hyndla'                =>           'Aktivere skrivertjenesten',
      'cast_to_hyndla_comment'        =>           'Sett til PÅ hvis du vil bruke bongskriver i systemet (krever abonnement)',
      'appServer_host'                =>           'Angi applikasjonsserver (adresse uten http)',
      'appServer_testHost'            =>           'Angi testserver (adresse uten http)',
      'appServer_testHost_isActive'   =>           'Angi om du vil kjøre mot testserver eller appserver',
      'opening_hours_open'            =>           'Åpningstid nettbestilling (daglig)',
      'opening_hours_open_comment'    =>           'Angi tidspunkt du vil begynne å ta imot bestillinger hver dag, skriv inn som "10:00" uten anførselstegn',
      'opening_hours_close'           =>           'Stengetid nettbestilling (daglig)',
      'opening_hours_close_comment'   =>           'Angi tidspunkt du vil slutte å ta imot bestillinger hver dag, skriv inn som "22:00" uten anførselstegn',
      'online_order_override'         =>           'Online bestilling overstyring',
      'online_order_override_comment' =>           'Angi om du vil overstyre vanlige åpningstider. NB! Du må huske å slå av overstyring ved stengetid, ellers er bestillingen alltid på!',
      'online_order_temporary_close'  =>           'Steng tjenesten midlertidig',
      'online_order_temporary_close_comment' =>    'Stenger tjenesten for bestilling. Husk å slå denne av ellers vil ingen kunne bestille!',
      'textBlock_menuWelcome'         =>           'Introtekst menyside',
      'textBlock_menuWelcome_comment' =>           'Tekst som vises for kunden i det de kommer inn på menyens førsteside. De fleste HTML-koder er lov.',
      'textBlock_menuOverworked'      =>           'Tekst for midlertidig stengt',
      'textBlock_menuOverworked_comment'      =>   'Teksten vises for kunden når tjenesten stenges midlertidig fra Dashboardet. Fint å bruke som en kort forklaring til at man har mye å gjøre, eller at noe annet har skjedd som gjør at man må stenge nettbestillingen midlertidig.',
      'conditions_use'                =>           'Generelle brukerbetingelser'
      ],

      'comment'                       => [
        'menu'                        => [
          'slug'                      =>           'Skriv inn menynavnet som url-sikker variant, bare små bokstaver uten mellomrom og spesialtegn.',
          'details'                   =>           'Her kan du skrive inn tekst som vises under menylistingen på nettsiden. De fleste HTML-koder er lov.',
          'details_top'               =>           'Her kan du skrive inn tekst som vises over menylistingen på nettsiden. De fleste HTML-koder er lov.',
          'products'                  =>           'Her linker du opp de varene som skal være en del av menyen. Varene sorteres med utgangspunkt i varens oppsatte sorteringsrekkefølge. Klikk på en vare for å gjøre endringer direkte.',
        ],
        'product'                     => [
          'time_to_prepare'           =>           'Tid i minutter det tar å få produktet klart til henting.',
          'active'                    =>           'Produktet vises ikke på nett før denne er aktivert.',
          'weborder'                  =>           'Produktet vises på nett, men kan ikke bestilles av kunde før denne er aktivert.',
          'sort_order'                =>           'Sorteringsrekkefølge internt i varegruppen. Bruk f.eks. 10-20-30 for å enkelt å legge til 21 og 22 senere.',
          'allergens'                 =>           'Skriv inn allergener med små bokstaver, separert med komma. Vises under hvert produkt i menyen.',
          'allergens_hide'            =>           'Nyttig hvis du har mange produkter med samme allergener og skriver dette i tekst over eller under menyen.'
        ],
        'productcategory'             => [
          'slug'                      =>           'Skriv inn varegruppenavnet som url-sikker variant, bare små bokstaver uten mellomrom og spesialtegn.',
          'sort_order'                =>           'Sorteringsrekkefølge blant varegruppene. Bruk f.eks. 10-20-30 for å enkelt å legge til 21 og 22 senere.'
        ],
        'schedule'                    => [
          'description'               =>           'En kort beskrivelse av menyen.',
          'sort_order'                =>           'Sorteringsrekkefølge blant timeplanene. Bruk gjerne hele titall for  å gi plass til nye menyer før og etter senere.'
        ],
        'specialdays'                 => [
          'closed'                    =>           'Sett denne til PÅ hvis du ønsker at hele dagen skal være stengt for nettbestilling.',
          'active'                    =>           'Denne må settes PÅ hvis du ønsker å overstyre vanlige åpningstider med hva du lagrer her. Lar du denne stå AV vil datoen lagres i systemet uten å være aktivert.',
          'publictext'                =>           'Tekst som vises på nettsiden som en forklaring til avvikende åpningstider eller hvorfor det er stengt.'
        ],
      ],




];
