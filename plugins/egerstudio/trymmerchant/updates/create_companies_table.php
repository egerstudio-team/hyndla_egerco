<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateCompaniesTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_companies', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->string('address');
            $table->string('zip');
            $table->string('city');
            $table->string('inv_address');
            $table->string('inv_zip');
            $table->string('inv_city');
            $table->string('phone');
            $table->string('email');
            $table->string('website');
            $table->string('company_id');
            $table->string('primary_contact');
            $table->string('primary_contact_phone');
            $table->string('primary_contact_email');
            $table->string('inv_email');
            $table->string('alias');
            $table->boolean('is_active');
            $table->nullableTimestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_companies');
    }

}
