<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateOrderlinesTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_orderlines', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('product_id')->unsigned()->index();
            $table->integer('order_id')->unsigned()->index();
            $table->decimal('price',15);
            $table->integer('quantity')->unsigned();
            $table->nullableTimestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_orderlines');
    }

}
