<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePrintServicesTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_print_services', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('type');
            $table->string('description');
            $table->text('code');
            $table->boolean('isActive');
            $table->nullableTimestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_print_services');
    }

}
