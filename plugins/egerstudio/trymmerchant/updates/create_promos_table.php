<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePromosTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_promos', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->boolean('active');
            $table->nullableTimestamps();
        });


        Schema::create('egerstudio_trymmerchant_promos_customers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('promos_id')->unsigned()->index();
            $table->integer('customers_id')->unsigned()->index();
            $table->boolean('active');
            $table->string('phone');
            $table->string('email');
            $table->nullableTimestamps();
        });

    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_promos');
        Schema::dropIfExists('egerstudio_trymmerchant_promos_customers');
    }

}
