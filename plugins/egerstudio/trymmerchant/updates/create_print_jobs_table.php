<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreatePrintJobsTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_print_jobs', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('order_id')->unsigned()->index();
            $table->integer('printservice_id')->unsigned()->index();
            $table->boolean('printed');
            $table->timestamp('printed_at')->nullable();
            $table->nullableTimestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_print_jobs');
    }

}
