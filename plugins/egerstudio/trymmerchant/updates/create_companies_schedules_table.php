<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateCompaniesSchedulesTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_companies_schedules', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('company_id');
            $table->integer('schedule_id');
            $table->boolean('is_active');
            $table->text('comment');
            $table->nullableTimestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_companies_schedules');
    }

}
