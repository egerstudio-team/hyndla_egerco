<?php namespace Egerstudio\Trymmerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateSpecialDaysTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_special_days', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamp('date')->nullable();
            $table->string('title');
            $table->text('comment');
            $table->timestamp('open')->nullable();
            $table->timestamp('close')->nullable();
            $table->boolean('closed');
            $table->boolean('active');
            $table->text('publictext');
            $table->nullableTimestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_special_days');
    }

}
