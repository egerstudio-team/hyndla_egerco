<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateMenusSchedulesTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_menus_schedules', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('description');
            $table->integer('sortOrder');
            $table->boolean('isActive');
            $table->integer('m_id')->unsigned()->index();
            $table->integer('s_id')->unsigned()->index();
            $table->nullableTimestamps();
            $table->primary(['m_id','s_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_menus_schedules');
    }

}
