<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateCompaniesCustomersTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_companies_customers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('company_id');
            $table->integer('customer_id');
            $table->text('comment');
            $table->nullableTimestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_companies_customers');
    }

}
