<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateOrderlinesTable extends Migration
{

    public function up()
    {
        Schema::table('egerstudio_trymmerchant_orderlines', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('schedule_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_orderlines');
    }

}
