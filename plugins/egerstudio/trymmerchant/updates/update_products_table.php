<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateProductsTable extends Migration
{

    public function up()
    {
        Schema::table('egerstudio_trymmerchant_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->boolean('weborder');
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_products');
    }

}
