<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateSettingsTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_settings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('settingsCartPage');
            $table->nullableTimestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_settings');
    }

}
