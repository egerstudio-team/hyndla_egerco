<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateDaysSchedulesTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_days_schedules', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('day_id');
            $table->integer('schedule_id');
            $table->nullableTimestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_days_schedules');
    }

}
