<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateProductCategoriesTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_product_categories', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('slug');
            $table->integer('sort_order');
            $table->boolean('active');
            $table->nullableTimestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_product_categories');
    }

}
