<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use DbDongle;
use October\Rain\Database\Updates\Migration;

class UpdateTimestampsNullable extends Migration
{

    public function up()
    {
        DbDongle::disableStrictMode();

        DbDongle::convertTimestamps('egerstudio_trymmerchant_companies');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_companies_customers');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_companies_schedules');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_customers');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_days_schedules');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_logs');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_menus');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_menus_products');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_menus_schedules');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_orderlines');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_print_services');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_product_categories');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_products');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_promos');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_promos_customers');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_settings');
        DbDongle::convertTimestamps('egerstudio_trymmerchant_schedules', ['created_at', 'updated_at', 'end_time', 'start_time']);
        DbDongle::convertTimestamps('egerstudio_trymmerchant_print_jobs', ['created_at', 'updated_at', 'printed_at']);
        DbDongle::convertTimestamps('egerstudio_trymmerchant_special_days', ['created_at', 'updated_at', 'open', 'close', 'date']);
    }

    public function down()
    {
        // ...
    }

}