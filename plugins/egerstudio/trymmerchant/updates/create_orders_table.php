<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateOrdersTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_orders', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('comment')->nullable();
            $table->string('verification_code')->nullable();
            $table->string('customer_code')->nullable();
            $table->boolean('verified');
            $table->boolean('confirmed');
            $table->boolean('verification_code_sent');
            $table->integer('customer_id')->unsigned()->index();
            $table->boolean('active');
            $table->boolean('deleted');
            $table->integer('remote_chain');
            $table->timestamp('pick_up_time')->nullable();
            $table->timestamp('picked_up')->nullable();
            $table->boolean('picked_up_ind');
            $table->string('trailing_title');
            $table->string('trailing_message');
            $table->integer('company_id')->unsigned()->index();
            $table->nullableTimestamps();
        });



    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_orders');
    }

}
