<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateOrdersProductsTable extends Migration
{

    public function up()
    {


        Schema::create('egerstudio_tymmerchant_orders_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('orders_id')->unsigned()->index();
            $table->integer('products_id')->unsigned()->index();
            $table->decimal('price',15)->default(0);
            $table->integer('quantity');
            $table->nullableTimestamps();
        });






    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_orders_products');
    }

}
