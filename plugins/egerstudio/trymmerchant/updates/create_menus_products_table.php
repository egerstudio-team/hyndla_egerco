<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateMenusProductsTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_menus_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('description');
            $table->integer('sortOrder');
            $table->boolean('isActive');
            $table->integer('m_id')->unsigned()->index();
            $table->integer('p_id')->unsigned()->index();
            $table->integer('specialPrice');
            $table->nullableTimestamps();
            $table->primary(['m_id','p_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_menus_products');
    }

}
