<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateDaysTable extends Migration
{

    public function up()
    {
        Schema::table('egerstudio_trymmerchant_days', function($table)
        {
            $table->engine = 'InnoDB';
            $table->string('open');
            $table->string('close');
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_days');
    }

}
