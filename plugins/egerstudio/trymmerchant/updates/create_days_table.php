<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateDaysTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_days', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('day');
            $table->integer('dayNo');
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_days');
    }

}
