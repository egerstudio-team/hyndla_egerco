<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateSchedulesTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_schedules', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->boolean('isActive');
            $table->boolean('disablePriceDisplay');
            $table->boolean('excemptFromRegular');
            $table->timestamp('start_time')->nullable();
            $table->timestamp('end_time')->nullable();
            $table->text('details');
            $table->integer('sort_order');
            $table->string('trailing_title');
            $table->string('trailing_text');
            $table->nullableTimestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_schedules');
    }

}
