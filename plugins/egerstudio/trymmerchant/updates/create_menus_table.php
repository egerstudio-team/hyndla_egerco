<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateMenusTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_menus', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->string('description');
            $table->string('slug');
            $table->boolean('isActive');
            $table->text('details');
            $table->text('details_top');
            $table->nullableTimestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_menus');
    }

}
