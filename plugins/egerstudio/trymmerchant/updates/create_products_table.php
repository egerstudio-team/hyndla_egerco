<?php namespace EgerStudio\TrymMerchant\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateProductsTable extends Migration
{

    public function up()
    {
        Schema::create('egerstudio_trymmerchant_products', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('allergens');
            $table->boolean('allergens_hide');
            $table->string('image');
            $table->decimal('price',15);
            $table->boolean('active');
            $table->integer('product_category_id')->unsigned()->index();
            $table->integer('sort_order');
            $table->boolean('weborder');
            $table->integer('time_to_prepare');
            $table->nullableTimestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('egerstudio_trymmerchant_products');
    }

}
