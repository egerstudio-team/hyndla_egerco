<?php namespace EgerStudio\TrymMerchant\Updates;

use Seeder;

class SeeDaysTable extends Seeder
{


    public function run()
    {
        $user = Db::table('egerstudio_trymmerchant_days')->insert(
                            array(
                              array('day' => 'Søndag','dayNo' => '0'),
                              array('day' => 'Mandag','dayNo' => '1'),
                              array('day' => 'Tirsdag','dayNo' => '2'),
                              array('day' => 'Onsdag','dayNo' => '3'),
                              array('day' => 'Torsdag','dayNo' => '4'),
                              array('day' => 'Fredag','dayNo' => '5'),
                              array('day' => 'Lørdag','dayNo' => '6'),
                          ));
    }

}
