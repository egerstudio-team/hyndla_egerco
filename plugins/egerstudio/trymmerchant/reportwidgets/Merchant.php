<?php namespace EgerStudio\TrymMerchant\ReportWidgets;

use Backend\Classes\ReportWidgetBase;
use EgerStudio\TrymMerchant\Models\trymMerchantSettings;
use EgerStudio\TrymMerchant\Models\Order;
use EgerStudio\TrymMerchant\Models\Log;
use EgerStudio\TrymMerchant\Classes\PrintService;
use Request;
use Log as SystemLog;
use Redirect;
use Mail;
use Carbon\Carbon;

class Merchant extends ReportWidgetBase
{



   public function render(){

      $orders = (new Order)->getOrders();
      $settings = trymMerchantSettings::instance();
      $open = $settings->get('online_order_temporary_close');

      return $this->makePartial('info', ['orders' => $orders, 'test' => 'basic','status' => $open]);
   }


   public function onConfirmOrder()
   {
     $order = Order::findOrFail(Request::input('orderId'));
     //var_dump($order);
     $order->confirmed = 1;
     $order->save();

     $log = new Log;
     $log->addLog(Request::input('orderId'),'Ordre '.Request::input('orderId').' ble bekreftet, melding sendes');

     //send confirmation message to client that the timing is OK and the order is recieved
     $order->sendIHaveSeenYou($order->id);

     $printService = new PrintService;
     $printService->sendOrderUpdate($order->id,'new',0);

     return Redirect::to('/backend/backend');
    }

    public function onPrintOrderCopy()
    {
      $order = Order::findOrFail(Request::input('orderId'));
      $printService = new PrintService;
      $printService->sendOrderUpdate($order->id,'new',1);
      $log = new Log;
      $log->addLog(Request::input('orderId'),'Kopi av ordre '.Request::input('orderId').' skrevet ut');
      return Redirect::to('/backend/backend');
    }


    public function onPickUp()
    {
      $order = Order::findOrFail(Request::input('orderId'));
      $order->picked_up = Carbon::now();
      $order->picked_up_ind = 1;
      $order->save();
      $log = new Log;
      $log->addLog(Request::input('orderId'),'Varene på ordre '.Request::input('orderId').' er hentet av kunde');


      return Redirect::to('/backend/backend');
     }



     public function onExtendOrder()
     {
       $order = Order::findOrFail(Request::input('orderId'));
       $time = Request::input('extendTime');
       $pickUpTime = Carbon::createFromFormat('Y-m-d H:i:s',$order->pick_up_time);
       $pickUpTime->addMinutes($time);
       $order->pick_up_time = $pickUpTime;
       $order->save();

       $log = new Log;
       $log->addLog($order->id,'Utsatt ordre '.$order->id.' til '.$order->pick_up_time);

       //only send the message if the order is confirmed, if not, we just extend, the client need to confirm the order after extending time
       if($order->confirmed == 1){
        //send confirmation message to client that the timing is OK and the order is recieved
        $order->sendUpdatedTime($order->id,'extend');
        $log = new Log;
        $log->addLog($order->id,'Melding om nytt tidspunkt på ordre '.$order->id.' til '.$order->pick_up_time.' sendt.');
        $printService = new PrintService;
        $printService->sendOrderUpdate($order->id,'update');
       }
       return Redirect::to('/backend/backend');
      }

      public function onShortenOrder()
      {
        $order = Order::findOrFail(Request::input('orderId'));
        $time = Request::input('shortenTime');
        $pickUpTime = Carbon::createFromFormat('Y-m-d H:i:s',$order->pick_up_time);
        $pickUpTime->subMinutes($time);
        $order->pick_up_time = $pickUpTime;
        $order->save();

        $log = new Log;
        $log->addLog(Request::input('orderId'),'Fremskyndet ordre '.Request::input('orderId').' til '.$order->pick_up_time);

        //only send the message if the order is confirmed, if not, we just extend, the client need to confirm the order after extending time
        if($order->confirmed == 1){
         //send confirmation message to client that the timing is OK and the order is recieved
         $order->sendUpdatedTime(Request::input('orderId'),'shorten');
         $log = new Log;
         $log->addLog(Request::input('orderId'),'Melding om nytt tidspunkt på ordre '.Request::input('orderId').' til '.$order->pick_up_time.' sendt.');
         $printService = new PrintService;
         $printService->sendOrderUpdate($order->id,'update',0);
        }
        return Redirect::to('/backend/backend');
       }


      public function onCustomMsg()
      {
        $order = Order::findOrFail(Request::input('orderId'));

        $log = new Log;
        $log->addLog(Request::input('orderId'),'Sender melding: '.Request::input('customMsg'));

        //send custom message to client
        $order->sendCustomMsg(Request::input('orderId'),Request::input('customMsg'));

        return Redirect::to('/backend/backend');
       }



     public function onRejectOrder()
     {
       $order = Order::findOrFail(Request::input('orderId'));
       $settings = trymMerchantSettings::instance();
       $order->active = 0;
       $order->save();
       if(Carbon::createFromFormat('Y-m-d H:i:s',$order->pick_up_time) > Carbon::now()) {
         $order->sendRejectOrder($order->id,' Vi beklager, men din ordre er dessverre kansellert. Mvh '.$settings->vanity_name_short);
       }

       $printService = new PrintService;
       $printService->sendOrderUpdate($order->id,'delete',0);

       $log = new Log;
       $log->addLog(Request::input('orderId'),'Ordre '.Request::input('orderId').' ble avvist.');

       return Redirect::to('/backend/backend');
      }

      public function onSystemOpen()
      {
        $settings = trymMerchantSettings::instance();
        SystemLog::info('Systemet åpent for ordremottak');
        $settings->online_order_temporary_close = 0;
        $settings->save();
        return Redirect::to('/backend/backend');
      }


      public function onSystemClose()
      {
        $settings = trymMerchantSettings::instance();
        SystemLog::info('Systemet lukket for ordremottak');
        $settings->online_order_temporary_close = 1;
        $settings->save();
        return Redirect::to('/backend/backend');
      }

}

