<?php namespace EgerStudio\TrymMerchant\Classes;

use Request;
use GuzzleHttp\Client;
use EgerStudio\trymMerchant\Models\trymMerchantSettings;
use EgerStudio\trymMerchant\Models\Orderline;
use EgerStudio\trymMerchant\Models\Order;
use EgerStudio\trymMerchant\Models\Product;
use EgerStudio\trymMerchant\Models\Log;
use Carbon;
use Log as SystemLog;

/**
 * Order Model
 */
class PrintService
{
      public function pollPrintServiceActive(){

        $verifyclient = new Client; // guzzle

        $settings = trymMerchantSettings::instance();
        if($settings->appServer_testHost_isActive == 1) {
          $appServerAddress = $settings->appServer_testHost;
          $vanityName = 'Hyndla Test';
        } else {
          $appServerAddress = $settings->appServer_host;
          $vanityName = $settings->vanity_name_short;
        }

        $verifyresponse = $verifyclient->request('GET',$appServerAddress.'/service/print/poll', [
          'query' => [
            'service' => 'print',
            'clientId' => $settings->client_id,
            'sKey' => $settings->secret_key,
            ]
          ]);

        $clientobj = $verifyresponse->getBody();
        $client = json_decode($clientobj);

        if($client->hasPrintService == '1') {
          return $client;
        } else {
          return false;
        }

      }


      // use this to send an order update or delete, action takes new, update or delete
      // set $duplicate to 1 in order to print 'THIS IS COPY' on the receipt
      public function sendOrderUpdate($orderId,$action,$duplicate=NULL){

        if($client = $this->pollPrintServiceActive()){
          $order = Order::where('id','=',$orderId)->with('customer')->with('company.schedules')->with('orderlines.product')->get();
          $this->doActualPrintJob($client, $order, $action,$duplicate);

        }
      }


      private function doActualPrintJob($client,$order,$action,$duplicate){
          SystemLog::info('Got as far as the actual printjob, order is '.$order);
          $settings = trymMerchantSettings::instance();

          $printclient = new Client;
          $location = $this->decideWhatToPrint($action,$client);

        try{

          $printOrder = $printclient->request('POST', 'http://'.$client->apiServer.$location,[
            'form_params' => [
              'content' => urlencode($order),
              'vpnIp' => $client->vpnIp,
              'meta' => http_build_query($settings->getMeta()),
              'apiKey' => $client->apiKey,
              'duplicate' => $duplicate,
            ]
          ]);

          //we sent a request, let us log what we got back
          $log = new Log;
          $log->addLog($order,'HyndlaAPI sier: '.$printOrder->getStatusCode());
          $log->addExtendedLog($order,'Sending to API: ',$printOrder->getBody()->getContents());
        } catch (Guzzle\Http\Exception\BadResponseException $e) {
              SystemLog::info('Something went wrong printing'.$e->getMessage());
        }


    }




      private function decideWhatToPrint($action,$client){

        switch ($action){

          case 'new':
          $location = $client->location_print_new;
          return $location;
          break;

          case 'update':
          $location = $client->location_print_update;
          return $location;
          break;

          case 'delete':
          $location = $client->location_print_delete;
          return $location;
          break;

        }
      }


      public function pollPrintService($orderId){
      // we have active print service wished by client, check with balder to confirm service is active
      $verifyclient = new Client;

      $settings = trymMerchantSettings::instance();
      if($settings->appServer_testHost_isActive == 1) {
        $appServerAddress = $settings->appServer_testHost;
        $vanityName = 'HyndlaTest';
      } else {
        $appServerAddress = $settings->appServer_host;
        $vanityName = $settings->vanity_name_short;
      }


      $verifyresponse = $verifyclient->request('GET',$appServerAddress.'/service/print/poll', [
        'query' => [
          'service' => 'print',
          'clientId' => $settings->client_id,
          'sKey' => $settings->secret_key,
          ]
        ]);

      $clientobj = $verifyresponse->getBody()->getContents();
      $client = json_decode($clientobj);
      
      

      if($client->hasPrintService){
        SystemLog::info('Client has printservice, sending print job');
        $order = Order::where('id','=',$orderId)->with('customer')->with('company')->with('orderlines.product')->get();
        $log = new Log;
        $log->addLog($orderId,'Spurte Balder om Hyndla skrivertjeneste, tjenesten er aktiv ('.$verifyresponse->getStatusCode().')');

          $printclient = new Client;

          // add error handling to printclient
          // Add custom error handling to any request created by this client

          $response = $printclient->request('POST','http://'.$client->apiServer.$client->location_print_new,[
            'form_params' => [
              'content' => urlencode($order),
              'vpnIp' => $client->vpnIp,
              'meta' => http_build_query($settings->getMeta()),
              'apiKey' => $client->apiKey
            ],
            'exceptions' => FALSE
          ]);


            //we sent a request, let us log what we got back
            $log = new Log;
            $log->addExtendedLog($orderId,'HyndlaApi returkode '.$response->getStatusCode(),"HyndlaApi sier om utskriftsjobb: ".$response->getStatusCode()."\n ".$response->getBody());
        }

    }
}
