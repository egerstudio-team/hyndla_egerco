<?php namespace EgerStudio\TrymMerchant\Classes;

use Illuminate\Routing\Controller;
use Request;
use GuzzleHttp\Client;
use EgerStudio\trymMerchant\Models\trymMerchantSettings;
use Log as SystemLog;
use Input;

/**
 *
 */
class MetaService extends Controller
{



        /* simple function for returning meta information to external services */
        public function returnMeta() {

          $balderId = Input::get('balderId');
          $settings = trymMerchantSettings::instance();
          if($settings->client_id == $balderId) {
            $meta = $settings->getMeta();
            return $meta;
          } else {
            return false;
          }
        }

}
