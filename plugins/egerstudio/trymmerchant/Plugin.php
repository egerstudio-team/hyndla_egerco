<?php namespace EgerStudio\TrymMerchant;

use App;
use Backend;
use System\Classes\PluginBase;
use Illuminate\Foundation\AliasLoader;


/**
 * trymMerchant Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'egerstudio.trymmerchant::lang.plugin.name',
            'description' => 'egerstudio.trymmerchant::lang.plugin.description',
            'author'      => 'EgerStudio',
            'icon'        => 'icon-shopping-cart'
        ];
    }



    public function registerSettings(){
      return [
          'settings' => [
              'label'       => 'egerstudio.trymmerchant::lang.plugin.name',
              'description' => 'egerstudio.trymmerchant::lang.plugin.description',
              'icon'        => 'icon-shopping-cart',
              'class'       => 'EgerStudio\trymMerchant\Models\trymMerchantSettings',
              'order'       => 1
            ]
        ];
    }



    public function registerPermissions()
    {
        return [
            'egerstudio.trymmerchant.customers'  => ['tab' => 'egerstudio.trymmerchant::lang.settings.name', 'label' => 'egerstudio.trymmerchant::lang.permissions.manage_customers'],
            'egerstudio.trymmerchant.orders'  => ['tab' => 'egerstudio.trymmerchant::lang.settings.name', 'label' => 'egerstudio.trymmerchant::lang.permissions.manage_orders'],
            'egerstudio.trymmerchant.menus'  => ['tab' => 'egerstudio.trymmerchant::lang.settings.name', 'label' => 'egerstudio.trymmerchant::lang.permissions.manage_menus'],
            'egerstudio.trymmerchant.schedules'  => ['tab' => 'egerstudio.trymmerchant::lang.settings.name', 'label' => 'egerstudio.trymmerchant::lang.permissions.manage_schedules'],
            'egerstudio.trymmerchant.products'  => ['tab' => 'egerstudio.trymmerchant::lang.settings.name', 'label' => 'egerstudio.trymmerchant::lang.permissions.manage_products'],
            'egerstudio.trymmerchant.specialdays'  => ['tab' => 'egerstudio.trymmerchant::lang.settings.name', 'label' => 'egerstudio.trymmerchant::lang.permissions.manage_specialdays'],
            'egerstudio.trymmerchant.companies'  => ['tab' => 'egerstudio.trymmerchant::lang.settings.name', 'label' => 'egerstudio.trymmerchant::lang.permissions.manage_companies'],
        ];
    }


    public function registerReportWidgets(){
        return [
            'EgerStudio\trymMerchant\ReportWidgets\Merchant' => [
                'label'     => 'egerstudio.trymmerchant::lang.widget.merchant.label',
                'context'   => 'dashboard'
            ]
        ];
    }

    public function registerNavigation()
    {
        return [
            'trymMerchant' => [
                'label'       => 'egerstudio.trymmerchant::lang.plugin.name',
                'url'         => Backend::url('egerstudio/trymmerchant/orders'),
                'icon'        => 'icon-shopping-cart',
                'permissions' => ['egerstudio.trymmerchant.orders'],
                'order'       => 50,

                'sideMenu' => [
                    'orders' => [
                        'label'       => 'egerstudio.trymmerchant::lang.navigation.orders',
                        'icon'        => 'icon-credit-card',
                        'url'         => Backend::url('egerstudio/trymmerchant/orders'),
                        'permissions' => ['egerstudio.trymmerchant.orders']
                    ],

                    'customers' => [
                        'label'       => 'egerstudio.trymmerchant::lang.navigation.customers',
                        'icon'        => 'icon-user',
                        'url'         => Backend::url('egerstudio/trymmerchant/customers'),
                        'permissions' => ['egerstudio.trymmerchant.customers']
                    ],

                    'companies' => [
                        'label'       => 'egerstudio.trymmerchant::lang.navigation.companies',
                        'icon'        => 'icon-building',
                        'url'         => Backend::url('egerstudio/trymmerchant/companies'),
                        'permissions' => ['egerstudio.trymmerchant.companies']
                    ],

                    'products' => [
                        'label'       => 'egerstudio.trymmerchant::lang.navigation.products',
                        'icon'        => 'icon-cube',
                        'url'         => Backend::url('egerstudio/trymmerchant/products'),
                        'permissions' => ['egerstudio.trymmerchant.products']
                    ],

                    'productcategories' => [
                        'label'       => 'egerstudio.trymmerchant::lang.navigation.productcategories',
                        'icon'        => 'icon-cubes',
                        'url'         => Backend::url('egerstudio/trymmerchant/productcategories'),
                        'permissions' => ['egerstudio.trymmerchant.productcategories']
                    ],
                    'menus' => [
                        'label'       => 'egerstudio.trymmerchant::lang.navigation.menus',
                        'icon'        => 'icon-file-text-o',
                        'url'         => Backend::url('egerstudio/trymmerchant/menus'),
                        'permissions' => ['egerstudio.trymmerchant.menus']
                    ],
                    'schedules' => [
                        'label'       => 'egerstudio.trymmerchant::lang.navigation.schedules',
                        'icon'        => 'icon-table',
                        'url'         => Backend::url('egerstudio/trymmerchant/schedules'),
                        'permissions' => ['egerstudio.trymmerchant.schedules']
                    ],
                    'specialdays' => [
                        'label'       => 'egerstudio.trymmerchant::lang.navigation.specialdays',
                        'icon'        => 'icon-calendar',
                        'url'         => Backend::url('egerstudio/trymmerchant/specialdays'),
                        'permissions' => ['egerstudio.trymmerchant.specialdays']
                    ],

                ]
            ]
        ];
    }

    public function registerComponents()
    {
        return [
            'EgerStudio\TrymMerchant\Components\MerchantMenu' => 'merchantMenu',
            'EgerStudio\TrymMerchant\Components\MerchantCart' => 'merchantCart',
            'EgerStudio\TrymMerchant\Components\MerchantConfirmation' => 'merchantConfirmation',
            'EgerStudio\TrymMerchant\Components\MerchantPlacedOrder' => 'merchantPlacedOrder',

        ];
    }



}
