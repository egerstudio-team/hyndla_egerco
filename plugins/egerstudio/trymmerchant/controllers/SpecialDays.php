<?php namespace Egerstudio\Trymmerchant\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Special Days Back-end Controller
 */
class SpecialDays extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('EgerStudio.TrymMerchant', 'trymMerchant', 'specialdays');
    }
}