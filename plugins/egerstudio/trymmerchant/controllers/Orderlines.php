<?php namespace EgerStudio\TrymMerchant\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Orderlines Back-end Controller
 */
class Orderlines extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('EgerStudio.TrymMerchant', 'trymMerchant', 'orderlines');
    }
}