<?php namespace EgerStudio\TrymMerchant\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Product Categories Back-end Controller
 */
class ProductCategories extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('EgerStudio.TrymMerchant', 'trymMerchant', 'productcategories');
    }
}
