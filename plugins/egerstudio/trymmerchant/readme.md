## Hyndla Nettordre
Fra **Eger Studio**.
Versjon 1.0.22 sist oppdatert 26.02.2016

### Innhold
1. Innføring
2. Systemkrav
3. Kom i gang
  1. Sette opp systemet
  2. Varer
  3. Varegrupper
  4. Menyer
  5. Timeplaner
  6. Datoer
4. Hvordan bruke systemet
  1. Motta en ordre
  2. Bekrefte en ordre
  3. Endre en ubekreftet ordre
  4. Avvise en ordre
  5. Midlertidig stenge tjenesten
  6. Endre åpningstider
5. Innstillinger
  1. Innstillinger du ikke bør endre
  2. Innstillinger du kan endre
  3. Brukerbetingelser
  4. Tekster
6. Brukeradministrasjon
  1. Legge til en bruker
  2. Endre en bruker
  3. Fjerne en bruker
  4. Tilgangsrettigheter
