<?php namespace EgerStudio\TrymMerchant\Models;

use Model;



/**
 * Menu Model
 */
class Menu extends Model
{



    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_trymmerchant_menus';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
      'schedules' => [
        'EgerStudio\TrymMerchant\Models\Schedule',
        'table' => 'egerstudio_trymmerchant_menus_schedules',
        'pivot' => ['description','sortOrder','isActive'],
        'key' => 'm_id',
        'otherKey' => 's_id'
      ],
      'products' => [
        'EgerStudio\TrymMerchant\Models\Product',
        'table' => 'egerstudio_trymmerchant_menus_products',
        'pivot' => ['description','sortOrder','isActive','specialPrice'],
        'key' => 'm_id',
        'otherKey' => 'p_id'
      ]
      ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}
