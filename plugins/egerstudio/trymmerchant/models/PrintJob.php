<?php namespace EgerStudio\TrymMerchant\Models;

use Model;

/**
 * PrintJob Model
 */
class PrintJob extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_trymmerchant_print_jobs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = ['order' => ['EgerStudio\trymMerchant\Models\Order'],'printservice' => ['EgerStudio\trymMerchant\Models\PrintService']];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];




    /**
     * POLL PRINT JOB
     *
     * @var integer id
     * takes an order id and checks if the system has new print jobs to send back to the client
     */
    public function pollPrintJob($id){

      



    }




    /**
     * SEND PRINT JOB
     *
     * @var integer printjob_id
     * sends a print job back to the client based on the printjob id
     */
    public function sendPrintJob($printjob_id) {





    }



    /**
     * POLL PRINT JOB
     *
     * @var
     *
     */
    public function



}
