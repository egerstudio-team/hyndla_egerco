<?php namespace EgerStudio\TrymMerchant\Models;

use Model;
use Input;
use Session;

/**
 * Customer Model
 */
class Customer extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_trymmerchant_customers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['id'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = ['orders' => ['EgerStudio\trymMerchant\Models\Order']];
    public $belongsTo = [];
    public $belongsToMany = [
      'companies' => [
            'EgerStudio\TrymMerchant\Models\Company',
            'table' => 'egerstudio_trymmerchant_companies_customers',
            'key' => 'company_id',
            'otherKey' => 'customer_id',
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];



    public function updateDetails() {

      if(!$this->firstname && $this->firstname <> Input::get('firstname')) {
        $this->firstname = Input::get('firstname');
      }
      if(!$this->lastname && $this->lastname <> Input::get('lastname')) {
        $this->lastname = Input::get('lastname');
      }
      if(!$this->email && $this->email <> Input::get('email')) {
        $this->email = Input::get('email');
      }
      $this->save();

    }


    public function checkNeedForDetails() {

        if(!$this->firstname) {
          return true;
        }
        if(!$this->lastname) {
          return true;
        }
        if(!$this->email) {
          return true;
        }


    }





}
