<?php namespace EgerStudio\TrymMerchant\Models;

use Model;
use EgerStudio\TrymMerchant\Models\Product;

/**
 * ProductCategory Model
 */
class ProductCategory extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_trymmerchant_product_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = ['products' => ['EgerStudio\TrymMerchant\Models\Product']];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];



    /**
     * Function for getting active product categories
     *
     * requires nothing
     */
    public function getProductCategories()
    {
      $categories = ProductCategory::where('active','=','1')->with(array('products' => function($query)
      {
          $query->where('active','=','1');
      }))->orderBy('sort_order')->get();

      return $categories;

    }




}
