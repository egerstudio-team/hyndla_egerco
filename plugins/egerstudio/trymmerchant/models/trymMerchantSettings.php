<?php namespace EgerStudio\trymMerchant\Models;

use Model;

class trymMerchantSettings extends Model{


    public $implement = ['System.Behaviors.SettingsModel'];
    public $settingsCode = 'egerstudio_trymMerchant_settings';
    public $settingsFields = 'fields.yaml';


    public function getMeta(){

      $meta = array(
        'receiptName' => $this->vanity_name_receipt,
        'receiptWeb' => $this->vanity_name_web,
        'override' => $this->online_order_override,
        'client' => $this->client_id,
      );
      return $meta;
    }


}
