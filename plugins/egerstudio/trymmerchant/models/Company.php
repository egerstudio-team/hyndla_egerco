<?php namespace EgerStudio\TrymMerchant\Models;

use Model;

/**
 * Company Model
 */
class Company extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_trymmerchant_companies';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = ['orders' => ['EgerStudio\TrymMerchant\Models\Order']];
    public $belongsTo = [];
    public $belongsToMany = [
        'schedules' => [
            'EgerStudio\TrymMerchant\Models\Schedule',
            'table' => 'egerstudio_trymmerchant_companies_schedules',
            'key' => 'company_id',
            'otherKey' => 'schedule_id',
        ],
        'customers' => [
            'EgerStudio\TrymMerchant\Models\Customer',
            'table' => 'egerstudio_trymmerchant_companies_customers',
            'key' => 'company_id',
            'otherKey' => 'customer_id',
        ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}