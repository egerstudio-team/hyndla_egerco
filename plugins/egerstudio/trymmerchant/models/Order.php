<?php namespace EgerStudio\TrymMerchant\Models;

use Model;
use Session;
use Input;
use Redirect;
use Request;
use GuzzleHttp\Client;
use EgerStudio\trymMerchant\Models\trymMerchantSettings;
use EgerStudio\trymMerchant\Models\Orderline;
use EgerStudio\trymMerchant\Models\Customer;
use EgerStudio\trymMerchant\Models\Product;
use EgerStudio\trymMerchant\Models\Log;
use Mail;
use Carbon\Carbon;
use EgerStudio\trymMerchant\Classes\PrintService;


/**
 * Order Model
 */
class Order extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_trymmerchant_orders';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [''];

    protected $dates = ['pick_up_time'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = ['orderlines' => ['EgerStudio\trymMerchant\Models\Orderline'],'logs'=>['EgerStudio\trymMerchant\Models\Log'],'printjobs' => ['EgerStudio\trymMerchant\Models\PrintJob']];
    public $belongsTo = ['customer' => ['EgerStudio\trymMerchant\Models\Customer'], 'company' => ['EgerStudio\trymMerchant\Models\Company']];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


    /**
     * Function for getting orders
     *
     *
     */
    public function getOrders($get = 10)
    {
      $orders = Order::where('verified', '=', 1)->where('active','=',1)->where('picked_up_ind','<>','1')->take($get)->with('orderlines.product')->orderBy('created_at','desc')->get();
      return $orders;
    }


    /**
     * Function for getting new orders
     *
     *
     */
    public function newOrders()
    {
      return $count = Order::where('verified','=',1)->where('confirmed','<>',1)->where('active','=',1)->count();
    }


    /**
     * Function for getting total cancelled orders
     *
     *
     */
    public function cancelledOrders()
    {
      return $count = Order::where('active','=',0)->count();
    }

    /**
     * Function for getting total confirmed orders
     *
     *
     */
    public function confirmedOrders()
    {
      return $count = Order::where('verified','=',1)->where('confirmed','=',1)->where('active','=',1)->where('picked_up_ind','=',1)->count();
    }



    /**
     * Function for getting related orderlines
     *
     *
     */
    public function orderlines()
    {
      return $this->hasMany('EgerStudio\trymMerchant\Models\Orderline');
    }


    /**
     * Function for getting related customer
     *
     *
     */
    public function customer()
    {
      return $this->belongsTo('EgerStudio\trymMerchant\Models\Customer');
    }

    /**
     * Function for adding an order
     *
     * requires nothing, sets up session variables for the order and verification code
     */
    public function addOrder()
    {
      $orderToAdd = new Order;
      $orderToAdd->verification_code = $orderToAdd->generateCode();
      $orderToAdd->save();
      //sets up session variables with the session order id and session generated code
      Session::put('orderId',$orderToAdd->id);
      Session::put('verificationCode',$orderToAdd->verification_code);
    }


    /**
     * Function for removing an order with its associated order lines
     *
     * requires the order id, flushes the session variables for order id and verification code
     */
    public function removeOrder($orderId)
    {

      //session flused, variables deleted
      Session::flush();
      $orderToRemove = Order::findOrFail($orderId);
      $orderToRemove->delete();

      // iterates the orderlines that have the associated order id and removes them
      $linestodelete = Orderline::where('order_id','=',$orderId)->get();
      foreach ($linestodelete as $line)
      {
          $line->delete();
      }
      $log = new Log;
      $log->addLog($orderId,'Slettet ordre '.$orderId);

    }





    /*
    *  generates a random code to send to the customer for verification
    *
    */
    public function generateCode($length = 5) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    /*
    *  sets confirmation when the order verification code is sent to the user
    *
    */
    public function verificationCodeSent($id) {
      $order = Order::findOrFail($id);
      $order->verificationCodeSent = 1;
      $log = new Log;
      $text = 'Verifiseringskode er opprettet ('.$order->verification_code.') og bekreftet sendt til kunde.';
      $log->addLog($id,$text);
      $order->save();
    }

    /*
    *  sends the verification code
    *
    */
    public function sendVerificationCode($mobile,$code){

      $client = new Client;
      $sendto = urlencode($mobile);
      $request = Request::instance();
      $ip = $request->getClientIp();
      $settings = trymMerchantSettings::instance();
      $shortName = $settings->vanity_name_short;

      $testChecked = $this->checkTestMode();
      $response = $client->request('POST','http://'.$testChecked['appServerAddress'].'/msg/recieve/', [
            'form_params' => [
                'clx'  => $settings->client_id,
                'sk'   => $settings->secret_key,
                'cx'   => $sendto,
                'ax'   => '210',
                'ip'   => $ip,
                'msg'  => 'Hei, din bekreftelseskode for bestilling er '.$code,
                'vns'  => $testChecked['vanityName'],
              ],
              'exceptions' => FALSE
          ]);
      return $response;


    }


    public function sendConfirmationMessage($mobile,$msg){

      $client = new Client;
      $sendto = urlencode($mobile);
      $request = Request::instance();
      $ip = $request->getClientIp();
      $settings = trymMerchantSettings::instance();

      $testChecked = $this->checkTestMode();

      $response = $client->request('POST','http://'.$testChecked['appServerAddress'].'/msg/recieve/', [
            'form_params' => [
                'clx'  => $settings->client_id,
                'sk'   => $settings->secret_key,
                'cx'   => $sendto,
                'ax'   => '211',
                'ip'   => $ip,
                'msg'  => $msg,
              ],
              'exceptions' => FALSE
          ]);
      return $response;
    }


    public function sendSupplierOrder($orderId,$msgId,$phone){


      /*$prettyfinishtime = $this->generatePickUpTime($orderId,1);

      $order = Order::findOrFail($orderId);
      $order->pick_up_time = date('Y-m-d H:i:s',$prettyfinishtime);
      $order->save();
      */


      if (trymMerchantSettings::get('cast_to_email') == TRUE){
         // send email
         $params = [
           'name' => trymMerchantSettings::get('vanity_name_long'),
           'orderId' => $orderId,
           'time' => date('H:i',$prettyfinishtime),
           'phone' => $phone,
         ];
         Mail::send('egerstudio.trymmerchant::mail.neworder', $params, function($message) {
           $message->to(trymMerchantSettings::get('cast_to_email_email'), trymMerchantSettings::get('vanity_name_long'));
        });
        $log = new Log;
        $log->addLog($orderId,'E-post sendt til '.trymMerchantSettings::get('cast_to_email_email').' med ordreinfo.');
      }


      // print service
      if(trymMerchantSettings::get('cast_to_hyndla') == TRUE){

        $cast = new PrintService;
        $cast->pollPrintService($orderId);
      }



      if(trymMerchantSettings::get('cast_to_phone') == TRUE) {
        $client = new Client;
        $sendto = trymMerchantSettings::get('cast_to_phone_number');
        $request = Request::instance();
        $ip = $request->getClientIp();
        $settings = trymMerchantSettings::instance();
        $chain = $msgId;

        $testChecked = $this->checkTestMode();



        $msg = 'Ny ordre med hentenummer '.$orderId.' skal være klart til henting kl. '.date('H:i',$prettyfinishtime).'. Bekreft via nettportalen snarest!';

        $response = $client->request('POST', 'http://'.$testChecked['appServerAddress'].'/msg/recieve/', [
              'form_params' => [
                  'clx'  => $settings->client_id,
                  'sk'   => $settings->secret_key,
                  'cx'   => $sendto,
                  'ax'   => '204',
                  'ip'   => $ip,
                  'msg'  => $msg,
                  'chx'  => $chain,
                  'vns' => $testChecked['vanityName'],
                  'bypassTestMode' => TRUE,
                ],
                'exceptions' => FALSE
            ]);
            $log = new Log;
            $log->addLog($orderId,'SMS sendt til '.trymMerchantSettings::get('cast_to_phone_number').' med ordreinfo.');
        return $response;
      }


  }



    public function sendIHaveSeenYou($orderId){



      $settings = trymMerchantSettings::instance();
      $client = new Client;
      $client2 = new Client;

      //get the customer
      $order = Order::findOrFail($orderId);
      $sendto = $order->customer->phone;
      $name = $order->customer->firstname;

      //set up confirmed time
      $time = $order->pick_up_time->format('H:i');

      $msg = urlencode('Hei '.$name.', vi kan bekrefte din ordre med hentenummer '.$order->id.'. Hentetidspunkt kl. '.$time.'. Med vennlig hilsen '.trymMerchantSettings::get('vanity_name_long'));

      $testChecked = $this->checkTestMode();

      $response = $client->request('POST','http://'.$testChecked['appServerAddress'].'/msg/recieve/', [
            'form_params' => [
                'clx'  => $settings->client_id,
                'sk'   => $settings->secret_key,
                'cx'   => $sendto,
                'ax'   => '215',
                'ip'   => '',
                'msg'  => $msg,
                'chx'  => $order->remote_chain,
                'vns' => $testChecked['vanityName'],
              ],
              'exceptions' => FALSE
          ]);


      $log = new Log;
      $log->addLog($orderId,'SMS-bekreftelse for ordre '.$orderId.' er sendt.');
      return $response;
    }


    public function sendUpdatedTime($orderId,$addOrRemove){

      $settings = trymMerchantSettings::instance();
      $client = new Client;

      //get the customer
      $order = Order::findOrFail($orderId);
      $sendto = $order->customer->phone;


      //set up confirmed time
      $time = date('H:i',strtotime($order->pick_up_time));

      if($addOrRemove=='shorten') {
        $msg = 'Hei, din ordre med hentenummer '.$order->id.' kan hentes kl. '.$time.'. Hilsen '.trymMerchantSettings::get('vanity_name_long');
      } else {
        $msg = 'Hei, din ordre med hentenummer '.$order->id.' er utsatt til kl. '.$time.'. Hilsen '.trymMerchantSettings::get('vanity_name_long');
      }



      $testChecked = $this->checkTestMode();

      $response = $client->request('POST', 'http://'.$testChecked['appServerAddress'].'/msg/recieve/', [
            'form_params' => [
                'clx'  => $settings->client_id,
                'sk'   => $settings->secret_key,
                'cx'   => $sendto,
                'ax'   => '216',
                'ip'   => '',
                'msg'  => $msg,
                'chx'  => $order->remote_chain,
                'vns' => $testChecked['vanityName'],
              ],
              'exceptions' => FALSE
          ]);
      $log = new Log;
      $log->addLog($orderId,'Oppdatert tidspunkt for ordre '.$orderId.', melding er sendt til kunde.');
      return $response;
    }



    public function sendRejectOrder($orderId,$incMsg){

      $settings = trymMerchantSettings::instance();
      $client = new Client;

      //get the customer
      $order = Order::findOrFail($orderId);
      $sendto = $order->customer->phone;

      $testChecked = $this->checkTestMode();


      $msg = 'ORDRE KANSELLERT: '.$incMsg;


      $response = $client->request('POST', 'http://'.$testChecked['appServerAddress'].'/msg/recieve/', [
            'form_params' => [
                'clx'  => $settings->client_id,
                'sk'   => $settings->secret_key,
                'cx'   => $sendto,
                'ax'   => '310',
                'ip'   => '',
                'msg'  => $msg,
                'chx'  => $order->remote_chain,
                'vns' => $testChecked['vanityName'],
              ],
              'exceptions' => FALSE
          ]);
          $log = new Log;
          $log->addLog($orderId,'SMS-info for AVVIST ordre '.$orderId.' er sendt til kunde.');
      return $response;

    }



    private function checkTestMode() {

      $settings = trymMerchantSettings::instance();

      if($settings->appServer_testHost_isActive == 1) {
        $appServerAddress = $settings->appServer_testHost;
        $vanityName = 'HyndlaTest';
      } else {
        $appServerAddress = $settings->appServer_host;
        $vanityName = $settings->vanity_name_short;
      }

      return array('appServerAddress' => $appServerAddress, 'vanityName' => $vanityName);

    }


    public function generatePickUpTime($orderId,$unformatted = NULL){

      //$timeHighest = Orderline::where('order_id','=',$orderId)->with('products')->orderby('time_to_prepare','desc')->take('1')->get();

      $timeHighest = Product::whereHas('orderlines', function($q) use ($orderId) {
          $q->where('order_id','=',$orderId);
        })->orderBy('time_to_prepare','desc')->firstOrFail();

      $seconds = time();
      $timetofinish = $seconds + (($timeHighest->time_to_prepare)*60);
      $finishtime = round($timetofinish / (10 * 60)) * (10 * 60);
      $prettyfinishtime = date('H:i',$finishtime);
      if($unformatted) {
        return $finishtime;
      } else {
        return $prettyfinishtime;
      }

    }


    public function getLatestPossiblePickUpTime($orderId) {

      $lastTime = Schedule::whereHas('orderlines', function($q) use ($orderId) {
        $q->where('order_id','=',$orderId);
      })->orderBy('end_time','desc')->first();

      $lastTimeToReturn = $lastTime->end_time;

      return $lastTimeToReturn;

       



    }

    public function getPickUpTimeValues($orderId) {

      $order = new Order;

      $startTime = $order->generatePickUpTime($orderId);

      // make sure we have a special day if that exists, otherwise we can 
      $specialDay = new SpecialDay;
      if($isSpecial = $specialDay->getSpecialDay()) {
        $endTime = $isSpecial->close->format('H:i');
      } else {
        $endTime = $order->getLatestPossiblePickUpTime($orderId);
      }
      
      
      
      $start = new Carbon($startTime);
      $end = new Carbon($endTime);

      

      
      $timeStep   = 10;
      $timeArray  = array();

      while($start <= $end)
      {
          $timeArray[] = $start->format('H:i');
          $start->add(new \DateInterval('PT'.$timeStep.'M'));
      }

      return $timeArray;
      


    }

    




}
