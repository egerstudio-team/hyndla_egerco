<?php namespace EgerStudio\TrymMerchant\Models;

use Model;

/**
 * PrintService Model
 */
class PrintService extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_trymmerchant_print_services';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = ['printjobs' => ['EgerStudio\trymMerchant\Models\PrintJob']];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}
