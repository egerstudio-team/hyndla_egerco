<?php namespace Egerstudio\Trymmerchant\Models;

use Model;
use Carbon\Carbon;

/**
 * specialDay Model
 */
class SpecialDay extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_trymmerchant_special_days';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $dates = ['date','open','close'];


    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];


    public function getSpecialDay(){
        $today = $this->whereDate('date','=',(new Carbon)->today())->where('active','=',1)->first();
        return $today;
    }



    // public function setOpenAttribute($value) {
    //     // Create date for carbon
    //     $time = date('Y-m-d H:i:s', strtotime('0-0-0 ' . $value));
    //     $this->attributes['open'] = Carbon::createFromFormat('Y-m-d H:i:s',$time);
    // }

    // public function setCloseAttribute($value) {
    //     // Create date for carbon
    //     $time = date('Y-m-d H:i:s', strtotime('0-0-0 ' . $value));
    //     $this->attributes['close'] = Carbon::createFromFormat('Y-m-d H:i:s',$time);
    // }




}