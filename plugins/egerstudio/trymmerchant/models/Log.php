<?php namespace EgerStudio\TrymMerchant\Models;

use Model;

/**
 * Log Model
 */
class Log extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_trymmerchant_logs';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['order_id','log_text','extended'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = ['order' => ['EgerStudio\trymMerchant\Models\Order']];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];



    public function addLog($orderId,$text)
    {
      $log = new Log;
      $log->order_id = $orderId;
      $log->log_text = $text;
      $log->save();
    }

    public function addExtendedLog($orderId,$title,$content)
    {
      $log = new Log;
      $log->order_id = $orderId;
      $log->log_text = $title;
      $log->extended = $content;
      $log->save();
    }

}
