<?php namespace EgerStudio\TrymMerchant\Models;

  use Model;
  use Carbon\Carbon;
  use EgerStudio\TrymMerchant\Models\Days;
  use EgerStudio\TrymMerchant\Models\Menu;
  use EgerStudio\TrymMerchant\Models\Product;
  use EgerStudio\TrymMerchant\Models\ProductCategory;
  

  /**
   * Schedule Model
   */
  class Schedule extends Model
  {

      /**
       * @var string The database table used by the model.
       */
      public $table = 'egerstudio_trymmerchant_schedules';

      /**
       * @var array Guarded fields
       */
      protected $guarded = ['*'];


      /**
       * @var array Fillable fields
       */
      protected $fillable = [];

      /**
       * @var array Relations
       */
      public $hasOne = [];
      public $hasMany = ['orderlines' => ['EgerStudio\TrymMerchant\Models\Orderline']];
      public $belongsTo = [];
      public $belongsToMany = [
        'menus' => [
          'EgerStudio\TrymMerchant\Models\Menu',
          'table' => 'egerstudio_trymmerchant_menus_schedules',
          'pivot' => ['description','sortOrder','isActive'],
          'key' => 's_id',
          'otherKey' => 'm_id'
        ],
        'days' => [
          'EgerStudio\TrymMerchant\Models\Days',
          'table' => 'egerstudio_trymmerchant_days_schedules',
          'key' => 'schedule_id',
          'otherKey' => 'day_id',
        ],
        'companies' => [
          'EgerStudio\TrymMerchant\Models\Company',
          'table' => 'egerstudio_trymmerchant_companies_schedules',
          'key' => 'schedule_id',
          'otherKey' => 'company_id',
        ],
        ];
      public $morphTo = [];
      public $morphOne = [];
      public $morphMany = [];
      public $attachOne = [];
      public $attachMany = [];


      public function beforeSave(){

        $this->start_time = date('Y-m-d H:i:s', strtotime('0-0-0 ' . $this->start_time));
        $this->end_time = date('Y-m-d H:i:s', strtotime('0-0-0 ' . $this->end_time));

        $this->dates[] = 'start_time';
        $this->dates[] = 'end_time';
      }

      public function getStartTimeAttribute($value) {
        return date('H:i',strtotime($value));
      }

      public function getEndTimeAttribute($value) {
        return date('H:i',strtotime($value));
      }




      public function getCurrentSchedules($category = NULL,$special = NULL)
      {

          if($category && $special) {
            $now = date('H:i');
            $today = Days::where('dayNo','=',Carbon::now()->dayOfWeek)->with(['schedules' => function ($query) use ($category) {
              $query->whereRaw("DATE_FORMAT(start_time,'%H:%i') < '".date('H:i')."'")
                    ->whereRaw("DATE_FORMAT(end_time,'%H:%i') > '".date('H:i')."'")
                    ->where('isActive','=','1')
                    ->where('excemptFromRegular','=','1')
                    ->orderBy('sort_order')
                    ->with(['menus' => function ($query) use ($category) {
                      $query->where('slug','=',$category)
                      ->orderBy('pivot_sortOrder')
                      ->with(['products' => function ($query) {
                        $query->where('active','=','1')
                        ->orderBy('sort_order')
                        ->with('product_category');
                        }]);
                    }]);
            }]);

              $today->get();
              return $today;


          } elseif ($category && !$special) {

            $now = date('H:i');
            $today = Days::where('dayNo','=',Carbon::now()->dayOfWeek)->with(['schedules' => function ($query) use ($category) {
            $query->whereRaw("DATE_FORMAT(start_time,'%H:%i') < '".date('H:i')."'")
                  ->whereRaw("DATE_FORMAT(end_time,'%H:%i') > '".date('H:i')."'")
                  ->where('isActive','=','1')
                  ->where('excemptFromRegular','=','0')
                  ->orderBy('sort_order')
                  ->with(['menus' => function ($query) use ($category) {
                    $query->where('slug','=',$category)
                    ->orderBy('pivot_sortOrder')
                    ->with(['products' => function ($query) {
                      $query->where('active','=','1')
                      ->orderBy('sort_order')
                      ->with('product_category');
                      }]);
                  }]);
            }]);
            $today->get();
            return $today;


          } elseif (!$category && !$special) {


            $now = date('H:i');
            $today = Days::where('dayNo','=',Carbon::now()->dayOfWeek)->with(['schedules' => function ($query) {
              $query->whereRaw("DATE_FORMAT(start_time,'%H:%i') < '".date('H:i')."'")
                    ->whereRaw("DATE_FORMAT(end_time,'%H:%i') > '".date('H:i')."'")
                    ->where('excemptFromRegular','=','0')
                    ->where('isActive','=','1')
                    ->orderBy('sort_order')
                    ->with(['menus' => function ($query) {
                      $query->orderBy('pivot_sortOrder')
                      ->with(['products' => function ($query) {
                        $query->where('active','=','1')
                        ->orderBy('sort_order')
                        ->with('product_category');
                        }]);
                    }]);
              }]);

              $today->get();
              return $today;


          }

      }


      public function getCurrentSchedulesMenu($category = NULL,$special = NULL)
      {

          if($category && $special) {
            $now = date('H:i');
            $today = Days::where('dayNo','=',Carbon::now()->dayOfWeek)->with(['schedules' => function ($query) use ($category) {
              $query->whereRaw("DATE_FORMAT(start_time,'%H:%i') < '".date('H:i')."'")
                    ->whereRaw("DATE_FORMAT(end_time,'%H:%i') > '".date('H:i')."'")
                    ->where('isActive','=','1')
                    ->where('excemptFromRegular','=','1')
                    ->orderBy('sort_order')
                    ->with(['menus' => function ($query) use ($category) {
                      $query->where('slug','=',$category)
                      ->orderBy('pivot_sortOrder')
                      ->with(['products' => function ($query) {
                        $query->where('active','=','1')
                        ->orderBy('sort_order')
                        ->with('product_category');
                        }]);
                    }]);
              }]);

              $today->get();
              return $today;


          } else {


            $now = date('H:i');
            $today = Days::where('dayNo','=',Carbon::now()->dayOfWeek)->with(['schedules' => function ($query) {
              $query->whereRaw("DATE_FORMAT(start_time,'%H:%i') < '".date('H:i')."'")
                    ->whereRaw("DATE_FORMAT(end_time,'%H:%i') > '".date('H:i')."'")
                    ->where('excemptFromRegular','=','0')
                    ->where('isActive','=','1')
                    ->orderBy('sort_order')
                    ->with(['menus' => function ($query) {
                      $query->orderBy('pivot_sortOrder')
                      ->with(['products' => function ($query) {
                        $query->where('active','=','1')
                        ->orderBy('sort_order')
                        ->with('product_category');
                        }]);
                    }]);
              }]);

              $today->get();
              return $today;


          }

      }

      /*
      //old, not used
      public function getLatestPickUpTime($orderId) {

        // get current schedules for right now
        // add products that are in current order_id
        // return the schedule to display last time for order pick-up
        // that would be last time + time to make

        /*$currentSched = Days::where('dayNo','=',Carbon::now()->dayOfWeek)->with(['schedules' => function ($query) use ($orderId) {
          $query->whereRaw("DATE_FORMAT(start_time,'%H:%i') < '".date('H:i')."'")
                ->whereRaw("DATE_FORMAT(end_time,'%H:%i') > '".date('H:i')."'")
                ->where('excemptFromRegular','=','0')
                ->where('isActive','=','1')
                ->orderBy('end_time','desc')
                ->with(['menus' => function ($query) use ($orderId) {
                  $query->orderBy('pivot_sortOrder')
                  ->with(['products' => function ($query) use ($orderId) {
                    $query->whereHas('orderlines', function($q) use ($orderId) {
                      $q->where('order_id','=',$orderId);})
                    ->where('active','=','1')
                    ->orderBy('sort_order')
                    ->with('product_category');
                    }]);
                }]);
          }])->get();

        return $currentSched;


        // get products in basket
        // get menus that products belong to


      }*/














  }
