<?php namespace EgerStudio\TrymMerchant\Models;

use Model;
use Session;
use Input;
use EgerStudio\TrymMerchant\Models\Product;
use EgerStudio\TrymMerchant\Models\Order;

/**
 * Orderline Model
 */
class Orderline extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_trymmerchant_orderlines';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
      'product' => ['EgerStudio\TrymMerchant\Models\Product', 'key'=> 'product_id'],
      'orders' => ['EgerStudio\TrymMerchant\Models\Order', 'key' => 'order_id'],
      'schedules' => ['EgerStudio\TrymMerchant\Models\Schedule', 'key' => 'schedule_id']
      ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];




    /**
     * Scope a query to only include current product id cast
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeProduct($query,$id)
    {
        return $query->where('product_id', $id);
    }



    /**
     * Scope a query to only include current order id cast
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOrder($query,$id)
    {
        return $query->where('order_id', $id);
    }




    /**
     * Function for getting current orders
     *
     * requires the order id and returns the orderlines as an array
     */
    public function getOrderLines()
    {
      $orderlines = Orderline::where('order_id','=',Session::get('orderId'))->get();
      return $orderlines;
    }


    public function getOrderLinesTotal()
    {
      $orderlines = Orderline::where('order_id','=',Session::get('orderId'))->get();
      $ordertotal = 0;

      foreach ($orderlines as $orderline)
      {
          $lineprice = $orderline->price*$orderline->quantity;
          $ordertotal += $lineprice;
      }

      return $ordertotal;
    }







    /**
     * Function for adding a line to an order
     *
     * requires the ids of an order, the product and the quantity to be added
     */
    public function addItem($orderId,$productId,$scheduleId,$quantity = 1)
    {
      // first we check to see if the orderline has an active parent order id, if not we add the order first
      if($orderId == NULL)
      {
        $order = new Order;
        $order->verification_code = $order->generateCode();
        $order->save();
        Session::put('orderId',$order->id);
        Session::put('verificationCode',$order->verification_code);
        $orderId = Session::get('orderId');
      }

      $productToAdd = Product::findOrFail($productId);

      // we need to check if we have the product already added in an orderline, if not we add a new orderline
      $existLine = Orderline::where('order_id',$orderId)->where('product_id',$productId)->first();
      if(!$existLine)
      {
        //there is no line, add a new orderline
        $addLine = new Orderline;
        $addLine->order_id = $orderId;
        $addLine->product_id = $productToAdd->id;
        $addLine->price = $productToAdd->price;
        $addLine->schedule_id = $scheduleId;
        // see if we have a specific quantity, otherwise we just add one to the value already there
        if($quantity > 1)
        {
          $addLine->quantity = $quantity;
        } else {
          $addLine->quantity++;
        }
        $addLine->save();
      } else {
        // there is already an orderline, let's update the quantity
        $addLine = Orderline::where('order_id',$orderId)->where('product_id',$productId)->first();
        // see if we have a specific quantity, otherwise we just add one to the value already there
        if($quantity > 1)
        {
          $addLine->quantity = $quantity;
        } else {
          $addLine->quantity++;
        }
        $addLine->save();
      }

    }


    /**
     * Function for removing a line from an order
     *
     * requires the id of the orderline to be removed
     */
    public function removeItem($orderlineId)
    {
      $removeLine = Orderline::findOrFail($orderlineId);
      $removeLine->delete();
    }


    /**
     * Function for updating an orderline
     *
     * requires the orderline id and the quantity
     */
    public function updateItem($orderId,$productId,$quantity=1)
    {
      $updateLine = Orderline::where('order_id',$orderId)->where('product_id',$productId)->first();
      // see if we have a specific quantity, otherwise we just add one to the value already there
      if($quantity > 1)
      {
        $updateLine->quantity = $quantity;
      } else {
        $updateLine->quantity--;
      }
      $updateLine->save();
    }












}
