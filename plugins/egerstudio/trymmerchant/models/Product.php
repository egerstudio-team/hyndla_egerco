<?php namespace EgerStudio\TrymMerchant\Models;

use Model;
use EgerStudio\TrymMerchant\Models\ProductCategory;

/**
 * Product Model
 */
class Product extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_trymmerchant_products';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = ['orderlines' => ['EgerStudio\trymMerchant\Models\Orderline']];
    public $belongsTo = ['product_category' => ['EgerStudio\trymMerchant\Models\ProductCategory']];
    public $belongsToMany = [
      'menus' => [
        'EgerStudio\TrymMerchant\Models\Menu',
        'table' => 'egerstudio_trymmerchant_menus_products',
        'key' =>  'p_id',
        'otherKey' => 'm_id'
        ]
      ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}
