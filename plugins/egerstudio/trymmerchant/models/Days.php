<?php namespace EgerStudio\TrymMerchant\Models;

use Model;

/**
 * Days Model
 */
class Days extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'egerstudio_trymmerchant_days';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
      'schedules' => [
        'EgerStudio\TrymMerchant\Models\Schedule',
        'table' => 'egerstudio_trymmerchant_days_schedules',
        'key' => 'day_id',
        'otherKey' => 'schedule_id',
      ],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}
