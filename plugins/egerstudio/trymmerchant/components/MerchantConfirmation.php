<?php namespace EgerStudio\TrymMerchant\Components;

use Cms\Classes\ComponentBase;
use EgerStudio\TrymMerchant\Models\ProductCategory;
use EgerStudio\TrymMerchant\Models\Order;
use EgerStudio\TrymMerchant\Models\Product;
use EgerStudio\TrymMerchant\Models\Orderline;
use EgerStudio\TrymMerchant\Models\Customer;
use EgerStudio\TrymMerchant\Models\Schedule;
use EgerStudio\TrymMerchant\Models\trymMerchantSettings;
use Carbon\Carbon;
use Session;
use Input;
use Redirect;
use Validator;
use Log;
use Response;

class MerchantConfirmation extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'merchantConfirmation Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
          'orderSummaryPage' => [
            'title' => 'Page for order summary',
            'description' => 'Name for the page the Order Summary component is placed on',
            'default' => '/kasse',
            'type' => 'string'
        ],
          'orderConfirmedPage' => [
            'title' => 'Page for placed order confirmation',
            'description' => 'Name for the page the placed order confirmation component is placed on',
            'default' => '/ordrebekreftet',
            'type' => 'string'
        ],
          'allowCompany' => [
            'title' => 'Allow company orders',
            'description' => 'Allow orders to be placed on behalf of companies',
            'type' => 'checkbox',
            'default' => 0
        ],
        ];
    }

    public function onRun()
    {
      // get the orderlines to put into the basket
      $orderlines = new Orderline;
      $schedule = new Schedule;
      $this->page['orderlines'] = $orderlines->getOrderLines();
      $this->page['ordertotal'] = $orderlines->getOrderLinesTotal();
      $this->page['orderSummaryPage'] = $this->property('orderSummaryPage');
      $this->page['disablePrice'] = $this->property('disablePrice');
      $this->page['allowCompany'] = $this->property('allowCompany');
      $order = Order::find(Session::get('orderId'));
      

      $settings = trymMerchantSettings::instance();
      $this->page['conditions'] = $settings->conditions_use;

      if($customer = Customer::find($order->customer_id)) {
        $getCustomerDetails = $customer->checkNeedForDetails();
      }
      

      $this->page['order'] = $order;
      $pickUpTime = new Carbon($order->pick_up_time);
      $this->page['pickUpTime'] = $pickUpTime->format('H:i');

      // check to see if we should show the comment or not
      if($order->comment <> ''){
        $this->page['showComment'] = true;
      }

      $this->page['phone'] = '';


      if(Session::get('verificationShow')==1)
      {
        $this->page['verificationShow'] = 1;
        $this->page['phone'] = Session::get('verificationPhone');
        $this->page['customer'] = $customer;
        if($getCustomerDetails) {
          $this->page['getCustomerDetails'] = 1;
        }
        if($this->property('allowCompany')) {
          if($customer){
            $this->page['companies'] = $customer->companies;
          }
        }


      } else {
        $this->page['verificationShow'] = 0;
        // we verified the customer, now check to see if we can show a company

      }
    }


    /*
    *  prepares the order for final step
    *
    */
    public function onPlaceOrder()
    {


      //verify the phone number first
      $validator = Validator::make(
          array('phone' => Input::get('phone')),
          array('phone' => array('required', 'digits:8'))
      );

      if(!$validator->fails() && Input::get('approve') == 1)
      {
          $order = Order::find(Session::get('orderId'));
          $phone = Input::get('phone');
          $company = Input::get('company');

          $customer = Customer::where('phone','=',$phone)->with('companies')->first();
          $this->page['customer'] = $customer;
          if(!count($customer)) {
            $customer = new Customer;
            $customer->phone = $phone;
            $customer->active = 1;
            $getCustomerDetails = 1;
            // we set session to register details
            $customer->save();
          } // we now have a customer in $customer->id;

          $order->customer_id = $customer->id;
          $order->active = 1;

          $order->save();
          $getCustomerDetails = $customer->checkNeedForDetails();
          $companies = $customer->companies;
          // all is done, prepared to send message, let's send the smsConfirmation

          $result = $order->sendVerificationCode($customer->phone,$order->verification_code);
          $checkresult = json_decode($result->getBody()->getContents(), true);
          Log::info($checkresult);
          //Log::info($order->pick_up_time);


          //check the result and see if we can confirm delivery
          if(is_array($checkresult['msg'])) {
            $this->page['checkresultmsg'] = $checkresult['msg'][0];
          } else {
            $this->page['checkresultmsg'] = $checkresult['msg'];
          }

          if($checkresult['status'] == 1)
          {
            Session::put('verificationShow',1);
            Session::put('verificationPhone',$customer->phone);
            // do some basic error checking on the xml we get back
            //message is sent, we can set the db variable
            $order = Order::find(Session::get('orderId'));
            $order->verification_code_sent = '1';
            $order->remote_chain = $checkresult['chx'];
            $order->save();
            Session::put('verificationShow',1);
            Session::put('verificationPhone',$phone);

            $this->page['phone'] = $customer->phone;
            if(isset($getCustomerDetails)) {
              return ['#verificationForm' => $this->renderPartial('merchantConfirmation::verification_input_details',['missing' => TRUE]),'#errors' => $this->renderPartial('merchantConfirmation::verification_errors')];
            } else {
              return ['#verificationForm' => $this->renderPartial('merchantConfirmation::verification_input',['companies' => $companies, 'allowCompany' => $this->property('allowCompany')]),'#errors' => $this->renderPartial('merchantConfirmation::verification_errors')];
            }
          } else {
            if(is_array($checkresult['msg'])) {
            $this->page['checkresultmsg'] = $checkresult['msg'][0];
            } else {
              $this->page['checkresultmsg'] = $checkresult['msg'];
            }
            return ['#errors' => $this->renderPartial('merchantConfirmation::verification_errors')];
          }

        } elseif (Input::get('approve') <> 1)  {
          $this->page['checkresultmsg'] = 'Du må godkjenne brukerbetingelsene.';
          return ['#errors' => $this->renderPartial('merchantConfirmation::verification_errors')];

        } else {
          $this->page['checkresultmsg'] = 'Ugyldig mobilnummer tastet inn.';
          return ['#errors' => $this->renderPartial('merchantConfirmation::verification_errors')];
        }






    }

    /*
    *  verify order code
    *
    */
    public function onVerifyCode()
    {
      $customerCode = Input::get('verCode');
      $order = Order::find(Session::get('orderId'));
      $customer = Customer::find($order->customer_id);
      $company = Input::get('company');
      $customer->updateDetails();




      if($order->verification_code == $customerCode)
      {
        // we have a match, update the db and redirect user to confirmed placed order page
        $order->customer_code = $customerCode;
        $order->verified = 1;
        $order->company = $company;
        $order->save();
        $order->sendSupplierOrder(Session::get('orderId'),$order->remote_chain,$order->customer->phone);
        Session::flush();
        Session::put('placedOrder',$order->id);

        // find the order to get the time
        $corder = Order::findOrFail(Session::get('placedOrder'));
        $findTime = new Carbon($corder->pick_up_time);
        $time = $findTime->format('H:i');
        Session::put('pickUpTime',$time);

        return Redirect::to($this->property('orderConfirmedPage'));

      } else {

        if(!$customer->firstname && !Input::get('firstname') && !Input::get('approve')) {
          $this->page['checkresultmsg'] = 'Du må fylle inn fornavn.';
          return ['#errors' => $this->renderPartial('merchantConfirmation::verification_errors')];
        } elseif (!$customer->lastname && !Input::get('lastname')) {
          $this->page['checkresultmsg'] = 'Du må fylle inn etternavn.';
          return ['#errors' => $this->renderPartial('merchantConfirmation::verification_errors')];
        } elseif (!$customer->email && !Input::get('email')) {
          $this->page['checkresultmsg'] = 'Du må fylle inn e-post adresse.';
          return ['#errors' => $this->renderPartial('merchantConfirmation::verification_errors')];
        }

        $this->page['checkresultmsg'] = 'Koden du tastet inn stemmer ikke med koden vi sendte deg. Vennligst forsøk igjen.';
        return ['#errors' => $this->renderPartial('merchantConfirmation::verification_errors')];
      }

    }


    /*
    *  removes an order from the database
    *  also removes the orderlines
    */
    public function onEmptyBasket()
    {

        $removeOrder = new Order;
        $removeOrder->removeOrder(Session('orderId'));
        $this->orderlines = array();
        $this->ordertotal = 0;

        return Redirect::to($this->property('orderSummaryPage'));

    }

    /*
    *  prepares the order confirmation
    *
    */
    public function onOrderSummary()
    {
      return Redirect::to($this->property('orderSummaryPage'));
    }


    

}
