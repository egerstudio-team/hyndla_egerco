<?php namespace EgerStudio\TrymMerchant\Components;

use Cms\Classes\ComponentBase;
use EgerStudio\TrymMerchant\Models\ProductCategory;
use EgerStudio\TrymMerchant\Models\Order;
use EgerStudio\TrymMerchant\Models\Product;
use EgerStudio\TrymMerchant\Models\Orderline;
use EgerStudio\TrymMerchant\Models\Menu;
use EgerStudio\TrymMerchant\Models\Schedule;
use EgerStudio\TrymMerchant\Models\SpecialDay;
use EgerStudio\TrymMerchant\Models\trymMerchantSettings;
use Input;
use Request;
use Session;
use Flash;
use Carbon\Carbon;

class MerchantMenu extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'merchantMenu Component',
            'description' => ''
        ];
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'       => 'egerstudio.trymmerchant::lang.settings.productcategory_slug',
                'description' => 'egerstudio.trymmerchant::lang.settings.productcategory_slug',
                'default'     => '{{ :slug }}',
                'type'        => 'string'
            ],
            'isSpecialCategory' => [
                'title'       => 'egerstudio.trymmerchant::lang.settings.productcategory_special',
                'description' => 'egerstudio.trymmerchant::lang.settings.productcategory_special_description',
                'type'        => 'checkbox',
                'default'     => 0
            ],
            'disablePrice' => [
                'title'       => 'egerstudio.trymmerchant::lang.settings.disablePrice',
                'description' => 'egerstudio.trymmerchant::lang.settings.disablePrice_description',
                'type'        => 'checkbox',
                'default'     => 0
            ],
            'specialCategory' => [
                'title'       => 'egerstudio.trymmerchant::lang.settings.specialCategory_slug',
                'description' => 'egerstudio.trymmerchant::lang.settings.specialCategory_slug_description',
                'default'     => '',
                'type'        => 'string'
            ],
            'categoryPage' => [
                'title'       => 'egerstudio.trymmerchant::lang.settings.categoryPage',
                'description' => 'egerstudio.trymmerchant::lang.settings.categoryPage_description',
                'default'     => '',
                'type'        => 'string'
            ],
            'confirmPage' => [
                'title'       => 'egerstudio.trymmerchant::lang.settings.confirmPage',
                'description' => 'egerstudio.trymmerchant::lang.settings.confirmPage_description',
                'default'     => '',
                'type'        => 'string'
            ],
        ];
    }



    /*
    *  on run we get the product categories and add in the products for displaying on the page
    *
    */
    public function onRender()
    {
      
      // get the orderlines to put into the basket
      $orderlines = new Orderline;
      $this->page['orderlines'] = $orderlines->getOrderLines();
      $this->page['ordertotal'] = $orderlines->getOrderLinesTotal();

      //opening hours
      $settings = trymMerchantSettings::instance();
      
      // make sure to get the opening hours of special days before we run the basic query
      $specialDay = new SpecialDay;
      
      if($this->page['specialDay'] = $specialDay->getSpecialDay()) {
        //we have a special day, override normal opening hours
        if($this->page['specialDay']->open) {
          $this->page['opening'] = Carbon::createFromFormat('H:i',$this->page['specialDay']->open->format('H:i'));
        } else {
          $this->page['opening'] = Carbon::createFromFormat('H:i','00:00');
        }
        if($this->page['specialDay']->close) {
          $this->page['closing'] = Carbon::createFromFormat('H:i',$this->page['specialDay']->close->format('H:i'));//  
        } else {
          $this->page['closing'] = Carbon::createFromFormat('H:i','00:00');
        }
        $this->page['openForBusiness'] = Carbon::now()->between($this->page['opening'],$this->page['closing']);
        $this->page['isSpecialDay'] = true;
        if($this->page['specialDay']->closed == 1) {
          // set the open for business to false and set a variable to prohibit menu listing when we are completely closed
          $this->page['openForBusiness'] = false;
          $this->page['noListMenu'] = true;
        } 
      } else {
        // normal day, nothing out of the ordinary, print opening hours as normal
        $this->page['opening'] = Carbon::createFromFormat('H:i',$settings->opening_hours_open);
        $this->page['closing'] = Carbon::createFromFormat('H:i',$settings->opening_hours_close);
        $this->page['openForBusiness'] = Carbon::now()->between($this->page['opening'],$this->page['closing']);
      }
      
      $this->page['welcomeText'] = $settings->textBlock_menuWelcome;
      $this->page['overworkedText'] = $settings->textBlock_menuOverworked;
      // overworked text is now set in the settings backend

      $this->page['isSpecialCategory'] = $this->property('isSpecialCategory');
      $this->page['specialCategory'] = $this->property('specialCategory');
      $this->page['disablePrice'] = $this->property('disablePrice');
      $this->page['categoryPage'] = $this->property('categoryPage');
      $this->page['slug'] = $this->property('slug');
      $this->page['confirmPage'] = $this->property('confirmPage');



      $schedule = new Schedule;

      if($this->page['isSpecialCategory']) {
        //run specialCategory
        $this->page['days'] = $schedule->getCurrentSchedules($this->page['specialCategory'],$this->page['isSpecialCategory']);
        $this->page['sideMenus'] = $schedule->getCurrentSchedulesMenu($this->page['specialCategory'],$this->page['isSpecialCategory']);
      } elseif ($this->page['slug']) {
        // we have category, show only that on subpage
        $this->page['days'] = $schedule->getCurrentSchedules($this->page['slug']);
        $this->page['sideMenus'] = $schedule->getCurrentSchedulesMenu($this->page['slug']);
      } else {
        $this->page['days'] = $schedule->getCurrentSchedules();
        $this->page['sideMenus'] = $schedule->getCurrentSchedulesMenu();
      }


      if($settings->online_order_override) {
        // online orders are always on, set value immediately
        $this->page['openForBusiness'] = 1;
        $this->page['temporaryClose'] = 0;
      }


      if($settings->online_order_temporary_close) {
        // the system is closed for new orders, set openForBusiness to 0;
        $this->page['temporaryClose'] = 1;
      }

      // if we have an orderId it's okay to show the confirmation
      if(Session::has('orderId'))
      {
        $this->page['confirmationShow'] = 1;
      }







    }


    /*
    *  adds an orderline to the database
    *  checks if we have a current order first, adds if necessary
    */
    public function onAddItem()
    {
      $orderline = new Orderline;
      $orderline->addItem(Session::get('orderId'),Input::get('productId'),Input::get('scheduleId'));
      $orderlines = new Orderline;
      $this->page['orderlines'] = $orderlines->getOrderLines();
      $this->page['ordertotal'] = $orderlines->getOrderLinesTotal();
      $this->page['disablePrice'] = $this->property('disablePrice');
    }


    /*
    *  removes an orderline from the basket
    *
    */
    public function onRemoveItem()
    {
      //get the orderline id, and then remove it
      $removeLine = new Orderline;
      $removeLine->removeItem(Input::get('orderlineId'));
      $orderlines = new Orderline;
      $this->page['orderlines'] = $orderlines->getOrderLines();
      $this->page['ordertotal'] = $orderlines->getOrderLinesTotal();
      $this->page['disablePrice'] = $this->property('disablePrice');
    }



    /*
    *  removes an order from the database
    *  also removes the orderlines
    */
    public function onEmptyBasket()
    {
      if(Session('orderId') !== NULL) {
        $removeOrder = new Order;
        $removeOrder->removeOrder(Session('orderId'));
        $this->page['orderlines'] = array();
        $this->page['ordertotal'] = 0;
        $this->page['disablePrice'] = $this->property('disablePrice');
      }
    }




}
