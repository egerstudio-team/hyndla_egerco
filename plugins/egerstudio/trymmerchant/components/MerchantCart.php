<?php namespace EgerStudio\TrymMerchant\Components;

use Cms\Classes\ComponentBase;
use EgerStudio\TrymMerchant\Models\ProductCategory;
use EgerStudio\TrymMerchant\Models\Order;
use EgerStudio\TrymMerchant\Models\Product;
use EgerStudio\TrymMerchant\Models\Orderline;
use EgerStudio\TrymMerchant\Models\Customer;
use EgerStudio\TrymMerchant\Models\Settings;
use EgerStudio\TrymMerchant\Models\SpecialDay;
use Session;
use Carbon\Carbon;
use Input;
use Redirect;


class MerchantCart extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'merchantCart Component',
            'description' => ''
        ];
    }

    public function defineProperties()
    {
        return [
          'orderConfirmationPage' => [
            'title' => 'Page for order confirmation',
            'description' => 'Name for the page the Order Confirmation component is placed on',
            'default' => '/ordrebekreftelse',
            'type' => 'string'
          ],
          'shoppingPage' => [
            'title' => 'Page for shopping items',
            'description' => 'Name of the page the menu component is placed on',
            'default' => '/meny',
            'type' => 'string'
          ],
          'allowCompany' => [
            'title' => 'Allow company orders',
            'description' => 'Allow orders to be placed on behalf of companies',
            'type' => 'checkbox',
            'default' => 0
        ],
        ];
    }




    public function onRun()
    {
      // get the orderlines to put into the basket
      $orderlines = new Orderline;
      $this->page['orderlines'] = $orderlines->getOrderLines();
      $this->page['ordertotal'] = $orderlines->getOrderLinesTotal();
      $this->page['disablePrice'] = $this->property('disablePrice');
      $this->page['orderConfirmationPage'] = $this->property('orderConfirmationPage');
      $order = Order::find(Session::get('orderId'));
      $this->page->order = $order;

      if(Session::has('orderId'))
      {
        $this->page['pickUpTime'] = $order->generatePickUpTime(Session::get('orderId'));
        $this->page['pickUpTimeValues'] = $order->getPickUpTimeValues(Session::get('orderId'));
        $this->page['confirmationShow'] = 1;
      } 
    }


    public function onQuantityChange()
    {
      $orderlineId = Input::get('orderlineId');
      $quantity = Input::get($orderlineId);
      $orderline = new Orderline;
      $orderline->updateItem(Session::get('orderId'),Input::get('productId'),$quantity);
      $orderlines = new Orderline;
      $this->page['orderlines'] = $orderlines->getOrderLines();
      $this->page['ordertotal'] = $orderlines->getOrderLinesTotal();
      $this->page['disablePrice'] = $this->property('disablePrice');

    }


    public function onQuantityAdd()
    {
      $orderline = new Orderline;
      $orderline->addItem(Session::get('orderId'),Input::get('productId'),Input::get('scheduleId'));
      $orderlines = new Orderline;
      $this->page['orderlines'] = $orderlines->getOrderLines();
      $this->page['ordertotal'] = $orderlines->getOrderLinesTotal();
      $this->page['disablePrice'] = $this->property('disablePrice');

    }


    public function onQuantitySubtract()
    {
      $orderline = new Orderline;
      $orderline->updateItem(Session::get('orderId'),Input::get('productId'));
      $orderlines = new Orderline;
      $this->page['orderlines'] = $orderlines->getOrderLines();
      $this->page['ordertotal'] = $orderlines->getOrderLinesTotal();
      $this->page['disablePrice'] = $this->property('disablePrice');

    }


    /*
    *  removes an orderline from the basket
    *
    */
    public function onRemoveItem()
    {
      //get the orderline id, and then remove it
      $removeLine = new Orderline;
      $removeLine->removeItem(Input::get('orderlineId'));
      $orderlines = new Orderline;
      $this->page['orderlines'] = $orderlines->getOrderLines();
      $this->page['ordertotal'] = $orderlines->getOrderLinesTotal();
      $this->page['disablePrice'] = $this->property('disablePrice');
    }



    /*
    *  removes an order from the database
    *  also removes the orderlines
    */
    public function onEmptyBasket()
    {
        $removeOrder = new Order;
        $removeOrder->removeOrder(Session('orderId'));
        $this->page['orderlines'] = array();
        $this->page['ordertotal'] = 0;
        $this->page['basket'] = 0;
        $this->page['disablePrice'] = $this->property('disablePrice');
    }



    /*
    *  prepares the order confirmation
    *
    */
    public function onOrderConfirmation()
    {
        $comment = Input::get('comment');
        $order = Order::find(Session::get('orderId'));
        $order->comment = $comment;
        $order->pick_up_time = new Carbon(Input::get('pickUpTime'));
        $order->save();
        return Redirect::to($this->property('orderConfirmationPage'));

    }

    /*
    *  sends the customer back to the shopping menu
    *
    */
    public function onContinueShopping()
    {
        return Redirect::to($this->property('shoppingPage'));

    }


    






}
