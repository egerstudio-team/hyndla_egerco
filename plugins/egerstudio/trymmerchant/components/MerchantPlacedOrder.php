<?php namespace EgerStudio\TrymMerchant\Components;

use Cms\Classes\ComponentBase;
use Session;

class MerchantPlacedOrder extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'merchantPlacedOrder Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
      $this->page->reference = Session::get('placedOrder');
      $this->page->time = Session::get('pickUpTime');
    }

}
